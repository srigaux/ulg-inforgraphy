//
//  ModelGroup.h
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#ifndef __GRAY__ModelGroup__
#define __GRAY__ModelGroup__

#include "Group.h"

using namespace std;

/**
 *
 * Classe permettant de regrouper des 'gmodel's
 * pour �tre anim�s ensemble.
 *
 * Voir Group.h pour commentaires non d�finis
 */
class ModelGroup : public Group {
protected:

    StringSet modelNames;
    
    string getDefaultName() const;
    void apply(const Gscene &scene, Transform t);
    
public:

    ModelGroup():
        Group(getDefaultName(), Point(0,0,0)),
        modelNames()
        {}

    ModelGroup(const ModelGroup& g):
        Group(g),
        modelNames(g.modelNames)
        {}

    /**
     * Initialise un ModelGroup contenant le gmodel 'model'
     */
    ModelGroup(const Gmodel& model);
    

    ModelGroup(string name, Point origin) :
        Group(name, origin),
        modelNames()
        {}
    
    ModelGroup(Point origin) :
        Group(getDefaultName(), origin),
        modelNames()
        {}

    Group* clone() const;
    
    /**
     * Ajoute un gmodel 'model' au groupe
     */
    void addModel(const Gmodel& model);
    void addSubGroup(const ModelGroup& group) { Group::addSubGroup(group); }

    /**
     * Ajoute un �l�ment (gmodel ou ModelGroup) au groupe
     */
    void add(const Gmodel& model) { addModel(model); }
    void add(const ModelGroup& group) { Group::addSubGroup(group); }

    /**
     * Retourne l'ensemble des noms des gmodels du groupe
     */
    StringSet getModelNames() const { return modelNames; }
};

#endif /* defined(__GRAY__ModelGroup__) */
