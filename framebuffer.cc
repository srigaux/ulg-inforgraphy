// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include <cmath>
#include <cstdlib>
#include <ostream>
#include <iostream>
#include <iomanip>
#include <vector>
#include "QuasiMonteCarlo.h"
#include "no_omp.h"
#include <png.h>


#include <string.h>
#include <stdarg.h>


#include "framebuffer.h"

void framebuffer::makeimage ( void )
{
    Gscene * sc = timeLine->getCurrentScene();
    
    time_t start = time ( 0 );
    srand ( start );
    sc->initCounter();
    
    int width = getwidth();
    int height = getheight();
    
    //Clear the buffer
    for ( int i = 0; i < width; i++ )
        for ( int j = 0; j < height; j++ )
            ( *this ) ( i,j ) = Color ( 0,0,0 );
    
    Camera* camera = sc->getCam();
    Point vectorLook = camera->getDirection() - sc->getViewpoint();
    vectorLook.normalize();
    VdcRandomizer vdcRandomizer;
    
    int counter = 0;
#pragma omp parallel
    {
        if ( omp_get_thread_num() == 0 )
        {
            std::cout << "Starting on " << omp_get_num_threads() << " threads. " << std::endl;
            std::cout << "|__________________________________________________|" << std::endl;
            std::cout << "|";
            std::cout.flush();
        }
    }
    
#ifdef USE_OMP_2_0
#pragma omp parallel for schedule(dynamic,10)
#else
#pragma omp parallel for collapse(3) schedule(dynamic,10)
#endif
    
    for ( int i = 0; i < width; i++ )
    {
        for ( int j = 0; j < height; j++ )
        {
            for ( int k = 0; k < sc->getOversampling(); k++ )
            {
                if ( omp_get_thread_num() == 0 && ( i*50 ) /width >= counter )
                {
                    std::cout << "=";
                    std::cout.flush();
                    counter++;
                }
                
                float x = i;
                float y = j;
                float dx = k==0?0:vdcRandomizer.getNext ( 3 )-0.5f;
                float dy = k==0?0:vdcRandomizer.getNext ( 5 )-0.5f;
                
                Ray r;
                get_coords ( x+dx,y+dy,r );
                Ray ray ( r.p, r.v*-1 );
                Color c;
                sc->raytracing ( ray,c );
                c = c * 1.0/sc->getOversampling();
#pragma omp critical
                {
                    ( *this ) ( i,j ) = ( *this ) ( i,j ) + c;
                }
            }
        }
    }
    
    empty = false;
    std::cout << "|" << std::endl;
    std::cout << "Computed in " << ( time ( 0 )-start ) << " seconds" << std::endl;
    std::cout << "Number of calls to getFirstIntersection() : " << sc->getCounter() << std::endl;
}

basic_framebuffer::basic_framebuffer (size_t w_,size_t h_ , TimeLine* tl) : w(w_), h(h_), framebuffer(tl), mem(w_ * h_)
{
    buffer=new Color[mem];
}

void basic_framebuffer::resize ( size_t w_,size_t h_ )
{
    size_t mem2=w_*h_;
    
    if ( mem2>mem )
    {
        delete buffer;
        mem=mem2;
        buffer=new Color[mem];
    }
    
    w=w_;
    h=h_;
    empty=true;
}

void basic_framebuffer::clear ( void )
{
    empty=true;
}

inline void setRGB(png_byte *ptr, Color val)
{
    ptr[0] = round(val.red*255.0);
    ptr[1] = round(val.green*255.0);
    ptr[2] = round(val.blue*255.0);
    //ptr[0] = 0;
    //ptr[1] = 0;//val.red;
    //ptr[2] = 255;//val.blue;
    //ptr[3] = 255;//val.green;
}

void abort_(const char * s, ...)
{
    va_list args;
    va_start(args, s);
    vfprintf(stderr, s, args);
    fprintf(stderr, "\n");
    va_end(args);
    abort();
}


void basic_framebuffer::save_image_png(const char* filename)
{
    //
    int x, y;
    
    int width, height;
    png_byte color_type = PNG_COLOR_TYPE_RGB;
    png_byte bit_depth = 8;
    
    png_structp png_ptr;
    png_infop info_ptr;
    //int number_of_passes;
    //png_bytep *row_pointers;
    png_bytep row;
    //
    width = w;
    height = h;
    
    
    
    //
    
    /* create file */
    FILE *fp = fopen(filename, "wb");
    if (!fp)
        abort_("[write_png_file] File %s could not be opened for writing \
                (Have you create the ./Movie folder ?)", filename);
    
    
    /* initialize stuff */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    
    if (!png_ptr)
        abort_("[write_png_file] png_create_write_struct failed");
    
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
        abort_("[write_png_file] png_create_info_struct failed");
    
    if (setjmp(png_jmpbuf(png_ptr)))
        abort_("[write_png_file] Error during init_io");
    
    png_init_io(png_ptr, fp);
    
    
    /* write header */
    if (setjmp(png_jmpbuf(png_ptr)))
        abort_("[write_png_file] Error during writing header");
    
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 bit_depth, color_type, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    
    png_write_info(png_ptr, info_ptr);
    
    
    /* write bytes */
    if (setjmp(png_jmpbuf(png_ptr)))
        abort_("[write_png_file] Error during writing bytes");
    
    
    
    row = (png_bytep) malloc(3 * width * sizeof(png_byte));
    
    // Write image data
    
    for (y=height - 1 ; y >= 0 ; y--) {
        for (x=0 ; x<width ; x++) {
            
            //fprintf(stderr,"value y %d x %d : R %f G %f B %f \n",y,x,buffer[y*width + x].red,buffer[y*width + x].green,buffer[y*width + x].blue);
            setRGB(&(row[x*3]), buffer[y*width + x]);
        }
        png_write_row(png_ptr, row);
    }
    
    // End write
    png_write_end(png_ptr, NULL);
    
    
    if (setjmp(png_jmpbuf(png_ptr)))
        abort_("[write_png_file] Error during end of write");
    
    png_write_end(png_ptr, NULL);
    
    free(row);
    fclose(fp);
}

void basic_framebuffer::save_image ( std::ostream& file )
{
    
    file.write ( ( char* ) &w,sizeof ( w ) );
    file.write ( ( char* ) &h,sizeof ( h ) );
    file.write ( ( char* ) buffer,sizeof ( Color ) *w*h );
}

void image_framebuffer::get_coods(float i, float j, Ray& ray)
{
    
    //int viewportInt[4] = {viewport[0], viewport[1], viewport[2], viewport[3]};
    
}
/*


*/