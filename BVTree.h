// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef BVTREE_H_
#define BVTREE_H_

#include "BoundingBox.h"
#include <iostream>
#include <math.h>
#include <list>

class Gscene;

class BVNode
{
public:
  BVNode() : info ( NULL ), left ( NULL ), right ( NULL ) {}
  BVNode ( BoundingBox* newInfo ) : info ( newInfo ), left ( NULL ), right ( NULL ) {}
  virtual ~BVNode() {};

  BVNode* getBVNodeLeft()
  {
    return left;
  }
  BVNode* getBVNodeRight()
  {
    return right;
  }
  BoundingBox* getBVNodeInfo()
  {
    return info;
  }

  void setBVNodeLeft ( BVNode* newLeft )
  {
    left = newLeft;
  }
  void setBVNodeRight ( BVNode* newRight )
  {
    right = newRight;
  }
  void setBVNodeInfo ( BoundingBox* newInfo )
  {
    info = newInfo;
  }

  bool isBVNodeLeaf()
  {
    return ( left == NULL ) && ( right == NULL );
  }

private:
  BoundingBox* info;
  BVNode* left;
  BVNode* right;
};


class BVTree
{
public:
  BVTree() : root ( NULL ) {}
  BVTree ( Gscene* );
  virtual ~BVTree();

  BVNode* getBVTreeRoot()
  {
    return root;
  }
  void setBVTreeRoot ( BVNode* newRoot )
  {
    root = newRoot;
  }

  void intersectionBBox ( BVNode*, Ray*, std::vector<Gmodel*>* );

private:
  BVNode* root;
  void freeBVTree ( BVNode* );
};

#endif /* BVTREE_H_ */
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
