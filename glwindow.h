// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __GLWINDOW_H_
#define __GLWINDOW_H_

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#ifdef WIN32
#include "windows.h"
#endif //WIN32
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif //_APPLE_

#include "glframebuffer.h"

// make a Gl window that draws something:
class MyGlWindow : public Fl_Gl_Window
{
public:
    unsigned int minframe;
    unsigned int maxframe;
    
  MyGlWindow ( int x, int y, int w, int h, gl_framebuffer *fb_ );
  void set_view ( void );
  void print_info ( void );
  void draw_pixmap ( void );
  void draw_model ( void );
  void add_lights ( void );
  void get_coords ( GLfloat i,GLfloat j, Ray & r );
  void get_matrices ( void );
    int handle ( int event );
private:
  gl_framebuffer *fb;
  void draw ( void ); // the draw method must be private
  void resize ( int X,int Y,int W,int H ) ; // the resize method must be private
  
  GLdouble ProjectionMatrix[4][4];
  GLdouble ModelViewMatrix[4][4];
  GLdouble Inv[4][4];
  GLint Viewport[4];
  void refreshViewpoint ( Point vp );
};

#endif //__GLWINDOW_H_
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
