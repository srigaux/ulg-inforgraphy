//
//  ModelGroup.h
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#ifndef __GRAY__ModelGroup__
#define __GRAY__ModelGroup__

#include "Group.h"

using namespace std;

class ModelGroup : public Group {
protected:
    StringSet modelNames;
    
    string getDefaultName() const;
    void apply(const Gscene &scene, Transform t);
    
public:

    ModelGroup():
        Group(getDefaultName(), Point(0,0,0)),
        modelNames()
        {}

    ModelGroup(const ModelGroup& g):
        Group(g),
        modelNames(g.modelNames)
        {}

    ModelGroup(const Gmodel& model);
    

    ModelGroup(string name, Point origin) :
        Group(name, origin),
        modelNames()
        {}
    
    ModelGroup(Point origin) :
        Group(getDefaultName(), origin),
        modelNames()
        {}

    Group* clone() const;
    
    void addModel(const Gmodel& model);
    void addSubGroup(const ModelGroup& group) { Group::addSubGroup(group); }

    void add(const Gmodel& model) { addModel(model); }
    void add(const ModelGroup& group) { Group::addSubGroup(group); }

    StringSet getModelNames() const { return modelNames; }
};

#endif /* defined(__GRAY__ModelGroup__) */
