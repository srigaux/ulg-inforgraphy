//
//  VertexGroup.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 15/04/14.
//
//

#include "VertexGroup.h"

string VertexGroup::getDefaultName() const {
    
    return "Vertex" + Group::getDefaultName();
}

Group* VertexGroup::clone() const {
    return new VertexGroup(*this);
}

void VertexGroup::addVertexIndex(int index) {
    vertexIndexes.insert(index);
}

void VertexGroup::addVertexIndexes(int *indexes, int count) {
    vertexIndexes.insert(indexes, indexes + count);
}

void VertexGroup::apply(const Gscene &scene, Transform t) {
    
    Point p = transformOrigin;
    Point r = t.apply(p);
    transformOrigin = r;
    
    
    Gmeshmodel * model = (Gmeshmodel*)scene.getModel(meshModelName);
    
    for (IntSet::iterator it = vertexIndexes.begin();
         it != vertexIndexes.end(); it++) {
        model->transform(*it, t);
    }
    
    for (StringSet::iterator it = subGroupsNames.begin();
         it != subGroupsNames.end(); it++) {
        VertexGroup * group = (VertexGroup*)scene.getGroup(*it);
        group->apply(scene, t);
    }
}

