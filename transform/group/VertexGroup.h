//
//  VertexGroup.h
//  GRAY
//
//  Created by Sébastien Rigaux on 15/04/14.
//
//

#ifndef __GRAY__VertexGroup__
#define __GRAY__VertexGroup__

#include "Group.h"

using namespace std;

typedef std::set<int> IntSet;

class VertexGroup : public Group {
    
protected:
    string meshModelName;
    IntSet vertexIndexes;
    
    string getDefaultName() const;
    
public:
    
    VertexGroup(const Gmeshmodel& meshModel):
        Group(getDefaultName(), Point(0,0,0)),
        meshModelName(meshModel.getName()),
        vertexIndexes()
        {}
    
    VertexGroup(const VertexGroup& g):
        Group(g),
        meshModelName(g.meshModelName),
        vertexIndexes(g.vertexIndexes)
        {}
    
    VertexGroup(Point origin, const Gmeshmodel& meshModel) :
        Group(getDefaultName(), origin),
        meshModelName(meshModel.getName()),
        vertexIndexes()
        {}
    
    VertexGroup(string name, Point origin, const Gmeshmodel& meshModel) :
        Group(name, origin),
        meshModelName(meshModel.getName()),
        vertexIndexes()
        {}
    
    Group* clone() const;
    
    void addVertexIndex(int index);
    void addVertexIndexes(int* indexes, int count);
    
    void addSubGroup(const VertexGroup& group) { Group::addSubGroup(group); }
    
    void add(int index) { addVertexIndex(index); }
    void add(const VertexGroup& group) { Group::add(group); }
    
    IntSet getVertexIndexes() const { return vertexIndexes; }
    
    void apply(const Gscene &scene, Transform t);
};


#endif /* defined(__GRAY__VertexGroup__) */
