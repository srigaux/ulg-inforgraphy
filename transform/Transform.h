//
//  AffineTransformation.h
//  GRAY
//
//  Created by Sébastien Rigaux on 9/03/14.
//
//

#ifndef __GRAY__Transform__
#define __GRAY__Transform__

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "../animation/Quaternion.h"

#include "../ray.h"

using namespace std;

inline double DegToRad(double x)
{
    return x / 180 * M_PI;
}

inline double RadToDeg(double x)
{
    return x / M_PI * 180.0f;
}

typedef struct
{
    double a, b, c, tx,
           d, e, f, ty,
           g, h, i, tz,
           p, q, r, s;
    
} TransformMatrix;

class Transform
{
    
private:
    
    TransformMatrix matrix;
    
    static double determinant2x2(double a, double b,
                                 double d, double e);
    
    static double determinant3x3(double a, double b, double c,
                                 double d, double e, double f,
                                 double g, double h, double i);
    
    static double determinant4x4(const Transform &t);
    
public:

    static const Transform identity;
    
    Transform();
    Transform(TransformMatrix matrix) : matrix(matrix) {}
    
    static Transform newTranslation(double tx, double ty, double tz);
    static Transform newScale(double s) { return newScale(s,s,s); };
    static Transform newScale(double sx, double sy, double sz);
    static Transform newRotation(double angle, double x, double y, double z);
    
    static Transform linearTransform(const Transform& ta, const Transform& tb, double fraction);
    
    TransformMatrix getMatrix() const { return matrix; }
    
    bool isIdentity() const;
    bool equalToTransform(const Transform &t) const;
    
    Transform concat(const Transform &t) const;
    Transform invert() const;
    Transform translate(double tx, double ty, double tz) const;
    Transform scale(double sx, double sy, double sz) const;
    Transform scale(double s) const { return scale(s,s,s); }
    
    Transform rotate(double radians, double x, double y, double z) const;
    Transform transpose() const;
    
    Transform atOrigin(double x, double y, double z) const;
    
    Transform linearTransform(double fraction);
    
    Point apply(const Point &point) const;
    
    friend ostream & operator<<(ostream & out, const Transform &t);
};

// For debugging:
void TransformPrint(Transform t);

#endif /* defined(__GRAY__AffineTransformation__) */
