// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//


#include <cmath>
#include <iostream>
#include "scene.h"
#include <cstdlib>

Gscene :: Gscene(Gscene& scene) :
    viewpoint(scene.viewpoint),
    camera(scene.camera),
    maxRecurLevel(scene.maxRecurLevel),
    Oversample(scene.Oversample),
    BlurLevel(scene.BlurLevel),
    tree(scene.tree),
    counter(scene.counter),
    #ifdef GRAY_HAVE_PHOTONMAP
        photonMap(scene.photonMap),
    #endif
    refractiveIndex(scene.refractiveIndex),
    typicalLightDist(scene.typicalLightDist),
    photonLighting(scene.photonLighting),
    directLighting(scene.directLighting)
{
    for (Gscene::iteratorModel it = scene.beginModel(); it != scene.endModel(); ++it)
    {
        Gmodel * m = (*it).second->clone();

        modlist[(*it).first] = m;
    }
    
    for (Gscene::iteratorGroup it = scene.beginGroup(); it != scene.endGroup(); ++it)
    {
        Group * g = (*it).second->clone();
        groupMap[(*it).first] = g;
    }

    for (Gscene::iteratorLight it = scene.beginLight(); it != scene.endLight(); ++it)
    {
        Glight * l = ((Glight*)*it)->clone();

        lightlist.push_back(l);
    }
}

Gscene :: ~Gscene()
{
    modlist.clear();
    groupMap.clear();

    while ( lightlist.begin() != lightlist.end() )
    {
        //printf("%p", lightlist.front());
        delete lightlist.front();
        lightlist.pop_front();
    }

    // Ajouté par le groupe 1
    if ( tree != NULL )
        delete tree;
}

void Gscene::insertModel(Gmodel* m)
{
    modlist[m->getName()] = m;
}

Gmodel * Gscene::getModel(string name) const
{
    ModelMap::const_iterator found = modlist.find(name);

    if (found == modlist.end())
        return NULL;

    return (*found).second;
}

void Gscene::insertGroup(Group *g)
{
    groupMap[g->getName()] = g;
}

Group * Gscene::getGroup(string name) const
{
    GroupMap::const_iterator found = groupMap.find(name);
    
    if (found == groupMap.end())
        return NULL;
    
    return (*found).second;
}

void Gscene :: insertLight ( Glight* l )
{
    lightlist.push_back ( l );
}

Gscene::iteratorModel Gscene::beginModel()
{
    return modlist.begin();
}

Gscene::iteratorModel Gscene::endModel()
{
    return modlist.end();
}

Gscene::iteratorGroup Gscene::beginGroup()
{
    return groupMap.begin();
}

Gscene::iteratorGroup Gscene::endGroup()
{
    return groupMap.end();
}

Gscene::iteratorLight Gscene::beginLight()
{
    return lightlist.begin();
}

Gscene::iteratorLight Gscene::endLight()
{
    return lightlist.end();
}

void Gscene::setViewpoint ( Point v )
{
    viewpoint = v;
}

Point Gscene::getViewpoint()
{
    return viewpoint;
}

bool Gscene::getFirstIntersection ( Ray &r, HitInfo &hitInfo )
{
    double t = 0;
    bool found =false;
    Point pre_n;

    for ( Gscene::iteratorModel it = beginModel(); it != endModel(); ++it )
    {
        Gmodel* m = (*it).second;
        double tt;

        counter++;

        if ( m->get_first_intersection ( r,tt,pre_n ) )
        {
            if ( !found || tt < t )
            {
                t = tt;
                found = true;
                hitInfo.object = m;
                hitInfo.point = r.p + r.v * t; // compute hitpoint
                hitInfo.ray = r;
                hitInfo.distance = t;
                m->normal ( hitInfo.point, pre_n, hitInfo.normal );  // compute normal at hitpoint

                Texture* normalMap = hitInfo.object->getMaterial()->getNormalMap();

                if ( normalMap != 0 )
                {
                    double u;
                    double v;

                    hitInfo.object->getUVFromPoint ( hitInfo.point, u, v, hitInfo.object->getMaterial()->getUTile(), hitInfo.object->getMaterial()->getVTile() );

                    int uReal = u * ( normalMap->getWidth() - 1 );
                    int vReal = v * ( normalMap->getHeight() - 1 );

                    int channels = normalMap->getChannelNumber();

                    int index = channels * ( vReal * normalMap->getWidth() + uReal );

                    int r = normalMap->getPixels() [index];
                    int g = normalMap->getPixels() [index + 1];
                    int b = normalMap->getPixels() [index + 2];

                    //Calcul à revoir
                    Point Tn ( 2 * ( r / 255.0 - 0.5 ), 2 * ( g / 255.0 - 0.5 ), 2 * ( b / 255.0 - 0.5 ) );

                    Point t;
                    m->tangent ( hitInfo.point, t, t );

                    Point n;
                    m->normal ( hitInfo.point, n, n );

                    Point bn;
                    m->binormal ( hitInfo.point, bn, bn );

                    double Nx = Tn.dotprod ( Point ( t.X, bn.X, n.X ) );
                    double Ny = Tn.dotprod ( Point ( t.Y, bn.Y, n.Y ) );
                    double Nz = Tn.dotprod ( Point ( t.Z, bn.Z, n.Z ) );

                    hitInfo.normal = Point ( Nx, Ny, Nz );
                    hitInfo.normal.normalize();
                }
            }
        }
    }

    return found;
}

void Gscene::raytracing ( Ray & r, Color & c )
{
    c = Color ( 0,0,0 );

    while ( r.level < maxRecurLevel && !r.kfactor.isBlack() )
    {
        HitInfo hitInfo;

        if ( !getFirstIntersection ( r, hitInfo ) ) break;

        //Couleur diffuse du point impacté
        Color localColor ( 0, 0, 0 );

        if ( directLighting ) shade ( localColor, hitInfo );

#ifdef GRAY_HAVE_PHOTONMAP

        if ( photonLighting ) photonMap.shade ( localColor, hitInfo );

#endif

        c = c+r.kfactor*localColor;

        if ( !hitInfo.object->getMaterial()->getNextRay ( hitInfo,r,*this ) ) break;
    }
}

/* Fonction de calcul de la couleur en un point (p)
 * sous l'eclairage de l'ensemble des sources lumineuses d'une scene
 * apres effets de diffusion/specularite
 */
void Gscene::shade ( Color & k, HitInfo & hitInfo )
{
    if ( !hitInfo.object->getMaterial()->isDiffusive() && !hitInfo.object->getMaterial()->isReflective() )
        return;

    for ( Gscene::iteratorLight it = beginLight(); it != endLight(); ++it )
    {
        Glight* light = *it;

        Point l = hitInfo.point - light->getPosition();
        Point v = hitInfo.ray.v;

        // distance between hitpoint and the current light
        double d = l.normalize();

        if ( d > 0 )
        {
            // new ray from light position towards hitpoint
            Ray lightRay ( light->getPosition(), l );

            // if the light face the surface and no object between the light and current hit object m
            if ( l * hitInfo.normal < 0 && !isInShadow ( lightRay, hitInfo.object, d ) )
            {
                Color factor;
                Color localColor;

                Texture* texture = hitInfo.object->getMaterial()->getTexture();

                if ( texture != 0 )
                {
                    double uCoord;
                    double vCoord;

                    hitInfo.object->getUVFromPoint ( hitInfo.point, uCoord, vCoord, hitInfo.object->getMaterial()->getUTile(), hitInfo.object->getMaterial()->getVTile() );

                    int uReal = uCoord * ( texture->getWidth() - 1 );
                    int vReal = vCoord * ( texture->getHeight() - 1 );

                    int channels = texture->getChannelNumber();

                    int index = channels * ( vReal * texture->getWidth() + uReal );

                    int red = texture->getPixels() [index];
                    int green = texture->getPixels() [index + 1];
                    int blue = texture->getPixels() [index + 2];

                    localColor = Color ( red / 255.0, green / 255.0, blue / 255.0 );

                    hitInfo.object->getMaterial()->brdf ( l,hitInfo.normal,v,localColor,factor );
                }
                else
                    hitInfo.object->getMaterial()->brdf ( l,hitInfo.normal,v,factor );

                k = k + factor * light->getColor() * light->getIntensity() / ( d*d ) *1E5;
            }
        }
    }
}

/* Fonction de test de l'intersection           */
/* d'un rayon d'ombre avec l'ensemble           */
/* des objets d'une scene.                      */
bool Gscene::isInShadow ( Ray &r, Gmodel * currentObj, double d )
{
    double dist;

    for ( Gscene::iteratorModel it = beginModel(); it != endModel(); ++it )
    {
        if ( (*it).second->get_first_intersection ( r,dist ) && dist < d - 1E-6 ) return true;
    }

    return false;
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
