//
//  KeyFrame.h
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#ifndef __GRAY__KeyFrame__
#define __GRAY__KeyFrame__

#include <iostream>
#include "Transform.h"
#include "EasingFunction.h"
#include "scene.h"

/**
 * Classe abstraite repr�sentant une KeyFrame
 *
 * Elle retient l'indice de sa frame de d�part, d'arriv�e
 * et sa dur�e.
 *
 * Elle force la d�finition de la m�thode 'clone'.
 */
class KeyFrame {
    
public:
    
	/**
	 * retourne un nouvel objet KeyFrame clone de l'objet courant
	 */
    virtual KeyFrame* clone() = 0;
    
    /**
     * retourne l'indice de la frame de d�part de l'animation
     */
    virtual unsigned int getStart() const = 0;

    /**
     * retourne la dur�e de l'animation (en frame)
     */
    virtual unsigned int getDuration() const = 0;
    
    /**
     * retourne l'indice de la frame d'arriv�e de l'animation
     */
    unsigned int getEnd() const {
        return getStart() + getDuration();
    }
    
    /**
     * retourne le pourcentage de compl�tude le la transformation
     * � �ffectuer pour la frame d'indice 'frame'
     */
    virtual double getInterpolationFractionForFrame(int frame) = 0;
    
    /**
     * Applique les transformations dont elle est responsable � la scene 'scene'
     * pour la frame d'indice 'frame'
     */
    virtual void apply(const Gscene& scene, int frame) = 0;
};

#endif /* defined(__GRAY__KeyFrame__) */
