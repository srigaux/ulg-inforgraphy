// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#include <map>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <set>
#include <vector>
#ifdef GRAY_HAVE_OGL
#ifdef WIN32
#include "windows.h"
#endif
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif //_APPLE_
#include "glu_gray.h"
#endif
#ifdef GRAY_HAVE_PLY
#include <cstdlib>
#include "ply.h"
#endif
#include "meshmodel.h"

#include <assert.h>

#define PI (3.14159265358979323846)

string Gmeshmodel::getDefaultName()
{
    return "Mesh" + Gmodel::getDefaultName();
}

Gmodel* Gmeshmodel::clone() const
{
    Gmeshmodel * m = new Gmeshmodel(*this);
    
    return m;
}

Gmeshmodel::Gmeshmodel(const Gmeshmodel& m) :
    Gmodel(m.name),
    linearInterpolation(m.linearInterpolation),
    mat(m.mat),
    min(m.min),
    max(m.max),
    vertices(m.vertices),
    normals(m.normals),
    adjEdges(m.adjEdges.size()),
    adjFaces(m.adjFaces.size()),
    triFaces(m.triFaces),
    quadFaces(m.quadFaces),
    vData(m.vData),
    maxCapacity(m.maxCapacity),
    alphas(m.alphas),
    betas(m.betas)
{
    int num_elems = m.adjEdges.size();
    adjEdges.resize (num_elems);
    
    for (int j = 0; j < num_elems; j++ )
        if (m.adjEdges[j] == NULL)
            adjEdges[j] = NULL;
        else
            adjEdges[j] = new AdjEdge(*(m.adjEdges[j]));
    
    num_elems = m.adjFaces.size();
    adjFaces.resize(num_elems);
    
    for (int j = 0; j < num_elems; j++ )
        if (m.adjFaces[j] == NULL)
            adjFaces[j] = NULL;
        else
            adjFaces[j] = new AdjFace(*m.adjFaces[j]);
    
    
    oTree = 0;
    
    this->buildOTree();
    this->compute_normals();
    
}

Gmeshmodel::Gmeshmodel(string newName, const Gmeshmodel& m) :
Gmodel(newName),
linearInterpolation(m.linearInterpolation),
mat(m.mat),
min(m.min),
max(m.max),
vertices(m.vertices),
normals(m.normals),
adjEdges(m.adjEdges.size()),
adjFaces(m.adjFaces.size()),
triFaces(m.triFaces),
quadFaces(m.quadFaces),
vData(m.vData),
maxCapacity(m.maxCapacity),
alphas(m.alphas),
betas(m.betas)
{
    int num_elems = m.adjEdges.size();
    adjEdges.resize (num_elems);
    
    for (int j = 0; j < num_elems; j++ )
        if (m.adjEdges[j] == NULL)
            adjEdges[j] = NULL;
        else
            adjEdges[j] = new AdjEdge(*(m.adjEdges[j]));
    
    num_elems = m.adjFaces.size();
    adjFaces.resize(num_elems);
    
    for (int j = 0; j < num_elems; j++ )
        if (m.adjFaces[j] == NULL)
            adjFaces[j] = NULL;
        else
            adjFaces[j] = new AdjFace(*m.adjFaces[j]);
    
    
    oTree = 0;
    
    this->buildOTree();
    this->compute_normals();
    
}

void Gmeshmodel::recalc()
{
    min = Point ( 1e90,1e90,1e90 );
    max = Point ( -1e90,-1e90,-1e90 );
    
    for (std::vector<Point>::iterator it = vertices.begin() ; it != vertices.end(); ++it) {
        min.X = ( (*it).X < min.X ) ? (*it).X : min.X;
        min.Y = ( (*it).Y < min.Y ) ? (*it).Y : min.Y;
        min.Z = ( (*it).Z < min.Z ) ? (*it).Z : min.Z;
        max.X = ( (*it).X > max.X ) ? (*it).X : max.X;
        max.Y = ( (*it).Y > max.Y ) ? (*it).Y : max.Y;
        max.Z = ( (*it).Z > max.Z ) ? (*it).Z : max.Z;
    }
    
    this->buildOTree();
    this->compute_normals();
}

void Gmeshmodel::transform(const Transform &t)
{
    for (std::vector<Point>::iterator it = vertices.begin() ; it != vertices.end(); ++it) {
        (*it) = t.apply(*it);
    }
    
    this->recalc();
}

void Gmeshmodel::transform(unsigned int vertexIndex, const Transform &t)
{
    Point p = vertices[vertexIndex];
    vertices[vertexIndex] = t.apply(p);
    
    this->recalc();
}

std::vector<int> Gmeshmodel::getVerticeIndexes(Point center, float radius) const
{
    std::vector<int> result;
    
    for (int i = 0; i < vertices.size(); i++) {
        Point p = vertices[i];
        
#define sq(x) ((x)*(x))
        
        float dist = sqrt(sq(center.X - p.X) +
                          sq(center.Y - p.Y) +
                          sq(center.Z - p.Z));
        
#undef sq
        
        if (dist <= radius)
            result.push_back(i);
    }
    
    return result;
}

// Computes the intersection with a ray.
bool Gmeshmodel::get_first_intersection ( Ray &r, double& t )
{
    Point n;
    return get_first_intersection ( r,t,n );
}


// Is the ray crossing the wrapping box ?
bool Gmeshmodel::boxIntersection ( Ray r, Point pmin, Point pmax )
{
    return ( ( r.v.X == 0 ) || ( ( pmin.X - r.p.X ) / r.v.X >= 0 )
            || ( ( pmax.X - r.p.X ) / r.v.X >= 0 ) )
    && ( ( r.v.Y == 0 ) || ( ( pmin.Y - r.p.Y ) / r.v.Y >= 0 )
        || ( ( pmax.Y - r.p.Y ) / r.v.Y >= 0 ) )
    && ( ( r.v.Z == 0 ) || ( ( pmin.Z - r.p.Z ) / r.v.Z >= 0 )
        || ( ( pmax.Z - r.p.Z ) / r.v.Z >= 0 ) );
}


// Computes the intersection with a ray and the normal on that point.
bool Gmeshmodel::get_first_intersection ( Ray &r, double& t, Point& n )
{
    if ( !boxIntersection ( r, min, max ) ) return false;
    
    std::set<int> intTri, intQuad;
    getIntersectedFaces ( 0, r, oTree, intTri, intQuad );
    
    t = 1; // intersection = P + t*V (with t < 0 and as close as possible).
    
    for ( std::set<int>::iterator a = intTri.begin(); a != intTri.end(); a++ )
    {
        int i = *a;
        
        // Check if Ray is parallel to Face.
        double s = -triFaces[i].n.dotprod ( r.v );
        
        if ( ( s > -0.00001 ) && ( s < 0.00001 ) ) continue;
        
        // k is the next possible t (then must be < 0 and closer than actual t).
        double k = ( triFaces[i].n.dotprod ( vertices[triFaces[i].vi[0]] - r.p ) ) / s;
        
        if ( ( k > 0 ) || ( t < 0 && k < t ) ) continue;
        
        // Computations for the following test.
        Point pInt = r.p - ( r.v * k );
        Point W = pInt - vertices[triFaces[i].vi[0]];
        double wu = W * triFaces[i].u;
        double wv = W * triFaces[i].v;
        double numU  = triFaces[i].vv * wu - triFaces[i].uv * wv;
        double numV  = triFaces[i].uu * wv - triFaces[i].uv * wu;
        
        // Test if the intersection is inside the triangle.
        if ( ( numU >= 0 ) && ( numV >= 0 ) && ( numU + numV <= triFaces[i].denum ) )
        {
            t = k;
            
            if ( linearInterpolation )
            {
                double n1 = numU / triFaces[i].denum;
                double n2 = numV / triFaces[i].denum;
                double n0 = 1 - n1 - n2;
                n = ( normals[triFaces[i].vi[0]] * n0 )
                + ( normals[triFaces[i].vi[1]] * n1 )
                + ( normals[triFaces[i].vi[2]] * n2 );
                n.normalize();
            }
            else
            {
                n = triFaces[i].n;
            }
        }
    }
    
    for ( std::set<int>::iterator a = intQuad.begin(); a != intQuad.end(); a++ )
    {
        int i = *a;
        // Does not compute intersection with a parallel ray
        Point tmp1 =  vertices[quadFaces[i].vi[1]] -
        vertices[quadFaces[i].vi[0]];
        
        Point tmp2 =  vertices[quadFaces[i].vi[3]] -
        vertices[quadFaces[i].vi[0]];
        
        double s = - ( tmp1.crossprod ( tmp2 ) ).dotprod ( r.v );
        
        if ( ( s > -0.00001 ) && ( s < 0.00001 ) ) continue;
        
        double A1 = ( quadFaces[i].a.X * r.v.Z ) - ( quadFaces[i].a.Z * r.v.X );
        double B1 = ( quadFaces[i].b.X * r.v.Z ) - ( quadFaces[i].b.Z * r.v.X );
        double C1 = ( quadFaces[i].c.X * r.v.Z ) - ( quadFaces[i].c.Z * r.v.X );
        double D1 = ( ( quadFaces[i].d.X - r.p.X ) * r.v.Z ) -
        ( ( quadFaces[i].d.Z - r.p.Z ) * r.v.X );
        
        double A2 = ( quadFaces[i].a.Y * r.v.Z ) - ( quadFaces[i].a.Z * r.v.Y );
        double B2 = ( quadFaces[i].b.Y * r.v.Z ) - ( quadFaces[i].b.Z * r.v.Y );
        double C2 = ( quadFaces[i].c.Y * r.v.Z ) - ( quadFaces[i].c.Z * r.v.Y );
        double D2 = ( ( quadFaces[i].d.Y - r.p.Y ) * r.v.Z ) -
        ( ( quadFaces[i].d.Z - r.p.Z ) * r.v.Y );
        
        double u, v, k;
        
        // Computes u, v, t
        // Solves Av² + Bv + C = 0
        double A = ( A2 * C1 ) - ( A1 * C2 );
        double B = ( A2 * D1 ) - ( A1 * D2 ) + ( B2 * C1 ) - ( B1 * C2 );
        double C = ( B2 * D1 ) - ( B1 * D2 );
        
        double v1, v2;
        int nbSol = solvesEquation ( A, B, C, v1, v2 );
        
        // No possible intersection
        if ( nbSol == 0 )
            continue;
        
        // One possible intersection
        if ( nbSol == 1 )
        {
            v = v1;
            
            if ( v >= 0 && v <= 1 )
            {
                u = computeU ( v, A1, B1, C1, D1, A2, B2, C2, D2 );
                
                if ( u >= 0 && u <= 1 )
                    k = computeT ( u, v, r, i );
                
                // Out of range
                else
                    continue;
            }
            
            // Out of range
            else
                continue;
        }
        
        // Two possible intersections
        else if ( nbSol == 2 )
        {
            double u1, u2, k1, k2;
            bool sol1 = true, sol2 = true;
            
            // Compute the first possible intersection
            if ( v1 >= 0 && v1 <= 1 )
            {
                u1 = computeU ( v1, A1, B1, C1, D1, A2, B2, C2, D2 );
                
                if ( u1 >= 0 && u1 <= 1 )
                    k1 = computeT ( u1, v1, r, i );
                
                // Out of range
                else
                    sol1 = false;
            }
            
            // Out of range
            else
                sol1 = false;
            
            // Compute the second possible intersection
            if ( v2 >= 0 && v2 <= 1 )
            {
                u2 = computeU ( v2, A1, B1, C1, D1, A2, B2, C2, D2 );
                
                if ( u2 >= 0 && u2 <= 1 )
                    k2 = computeT ( u2, v2, r, i );
                
                // Out of range
                else
                    sol2 = false;
            }
            
            // Out of range
            else
                sol2 = false;
            
            // Choose the best one (if possible)
            // No possible intersection
            if ( !sol1 && !sol2 )
                continue;
            
            // Choose the first intersection (the closest one)
            // Or the only possible intersection
            // The first solution is out of range
            if ( !sol1 )
            {
                u = u2;
                v = v2;
                k = k2;
            }
            
            // The second solution is out of range
            else if ( !sol2 )
            {
                u = u1;
                v = v1;
                k = k1;
            }
            // All solutions are in the correct range
            else
            {
                // The first solution is the first/closest intersection
                if ( k1 < k2 )
                {
                    u = u1;
                    v = v1;
                    k = k1;
                }
                
                // The second solution is the first/closest intersection
                else
                {
                    u = u2;
                    v = v2;
                    k = k2;
                }
            }
        }
        
        else
            continue;
        
        if ( ( k > 0 ) || ( t < 0 && k < t ) ) continue;
        
        t = k;
        
        // Compute the good normal
        if ( linearInterpolation )
        {
            n = ( normals[quadFaces[i].vi[0]] * ( 1 - u ) * ( 1 - v ) +
                 normals[quadFaces[i].vi[3]] * u       * ( 1 - v ) +
                 normals[quadFaces[i].vi[1]] * ( 1 - u ) * v       +
                 normals[quadFaces[i].vi[2]] * u       * v );
        }
        
        else
            n = quadFaces[i].n;
        
        n.normalize();
    }
    
    t=-t;
    return t >= 0;
}

// Solves the equation Ax² + Bx + C = 0
int Gmeshmodel::solvesEquation ( double A, double B, double C,
                                double& sol1, double& sol2 )
{
    if ( A == 0 )
    {
        if ( B != 0 )
        {
            solvesFirstDegreeEquation ( B, C, sol1 );
            return 1;
        }
        
        return ( C == 0 ) ? -1 : 0;
    }
    
    return solvesSecondDegreeEquation ( A, B, C, sol1, sol2 );
}

// Solves the equation Bx + C = 0 (B != 0)
void Gmeshmodel::solvesFirstDegreeEquation ( double B, double C, double& sol )
{
    sol = ( -C / B );
}

// Solves the equation Ax² + Bx + C = 0 (A != 0)
int Gmeshmodel::solvesSecondDegreeEquation ( double A, double B, double C,
                                            double& sol1, double& sol2 )
{
    double ro = ( B * B ) - ( 4 * A * C );
    
    // 0 solution
    if ( ro < 0 )
        return 0;
    
    // 1 solution
    if ( ro == 0 )
    {
        sol1 = ( - B ) / ( 2 * A );
        return 1;
    }
    
    // 2 solutions
    // ro > 0
    double sqrtRo = std::sqrt ( ro );
    sol1 = ( ( - B ) - sqrtRo ) / ( 2 * A );
    sol2 = ( ( - B ) + sqrtRo ) / ( 2 * A );
    return 2;
}

// Compute U for a bilinear patch according to V
double Gmeshmodel::computeU ( double v,
                             double A1, double B1, double C1, double D1,
                             double A2, double B2, double C2, double D2 )
{
    double a = ( v * A2 ) + B2;
    double b = ( v * ( A2 - A1 ) ) + B2 - B1;
    
    if ( std::abs ( b ) >= std::abs ( a ) )
        return ( v * ( ( C1 - C2 ) ) + D1 - D2 ) / b;
    
    else
        return ( ( -v * C2 ) - D2 ) / a;
}

// Compute the T value of the ray parametric equation (P(t) = r.p + T * r.v)
// that intersect the face number 'faceNb' at (u, v) position on the bilinear
// patch defined.
double Gmeshmodel::computeT ( double u, double v, Ray r, int faceNb )
{
    Point p =
    ( vertices[quadFaces[faceNb].vi[0]] * ( 1 - u ) * ( 1 - v ) +
     vertices[quadFaces[faceNb].vi[3]] * u       * ( 1 - v ) +
     vertices[quadFaces[faceNb].vi[1]] * ( 1 - u ) * v       +
     vertices[quadFaces[faceNb].vi[2]] * u       * v );
    
    if ( std::abs ( r.v.X ) >= std::abs ( r.v.Y ) &&
        std::abs ( r.v.X ) >= std::abs ( r.v.Z ) )
        return - ( p.X - r.p.X ) / r.v.X;
    
    else if ( std::abs ( r.v.Y ) >= std::abs ( r.v.Z ) )
        return - ( p.Y - r.p.Y ) / r.v.Y;
    
    else
        return - ( p.Z - r.p.Z ) / r.v.Z;
}


// Return the pre-calculated (by get_first_intersection) normal.
void Gmeshmodel::normal ( const Point&, const Point& pre_n, Point& n )
{
    n = pre_n;
}

void Gmeshmodel::tangent ( const Point& p, const Point& pre_t, Point& t )
{
    t = pre_t;
}

void Gmeshmodel::binormal ( const Point& p, const Point& pre_bn, Point& bn )
{
    bn = pre_bn;
}


// Computes normals, and other internal variables, for every vertices.
void Gmeshmodel::compute_normals()
{
    // Reset normals.
    normals.resize ( vertices.size() );
    
    for ( int i = 0; i < normals.size(); i++ )
        normals[i] = Point ( 0,0,0 );
    
    // Compute normals for triangles.
    for ( int i = 0; i < triFaces.size(); i++ )
    {
        triFaces[i].u = vertices[triFaces[i].vi[1]] - vertices[triFaces[i].vi[0]];
        triFaces[i].v = vertices[triFaces[i].vi[2]] - vertices[triFaces[i].vi[0]];
        triFaces[i].n = triFaces[i].u.crossprod ( triFaces[i].v );
        triFaces[i].n.normalize();
        triFaces[i].uu = triFaces[i].u * triFaces[i].u;
        triFaces[i].uv = triFaces[i].u * triFaces[i].v;
        triFaces[i].vv = triFaces[i].v * triFaces[i].v;
        triFaces[i].denum = triFaces[i].uu * triFaces[i].vv
        - triFaces[i].uv * triFaces[i].uv;
        
        for ( int j = 0; j < 3; ++j )
            normals[triFaces[i].vi[j]] = normals[triFaces[i].vi[j]] + triFaces[i].n;
    }
    
    // Compute normals for quads.
    for ( int i = 0; i < quadFaces.size(); i++ )
    {
        // Vectors used to compute the bilinear patch intersection
        quadFaces[i].a = vertices[quadFaces[i].vi[2]] -
        vertices[quadFaces[i].vi[3]] -
        vertices[quadFaces[i].vi[1]] +
        vertices[quadFaces[i].vi[0]];
        
        quadFaces[i].b = vertices[quadFaces[i].vi[3]] -
        vertices[quadFaces[i].vi[0]];
        
        quadFaces[i].c = vertices[quadFaces[i].vi[1]] -
        vertices[quadFaces[i].vi[0]];
        
        quadFaces[i].d = vertices[quadFaces[i].vi[0]];
        
        // Computes the quad normal
        Point u = vertices[quadFaces[i].vi[1]] - vertices[quadFaces[i].vi[0]];
        Point v = vertices[quadFaces[i].vi[3]] - vertices[quadFaces[i].vi[0]];
        Point w = vertices[quadFaces[i].vi[1]] - vertices[quadFaces[i].vi[2]];
        Point x = vertices[quadFaces[i].vi[3]] - vertices[quadFaces[i].vi[2]];
        
        Point n0 = u.crossprod ( v );
        Point n1 = ( w * ( -1 ) ).crossprod ( u * ( -1 ) );
        Point n2 = x.crossprod ( w );
        Point n3 = ( v * ( -1 ) ).crossprod ( x * ( -1 ) );
        
        quadFaces[i].n = n0 + n1 + n2 + n3;
        quadFaces[i].n.normalize();
        
        for ( int j = 0; j < 4; j++ )
            normals[quadFaces[i].vi[j]] = normals[quadFaces[i].vi[j]] +
            quadFaces[i].n;
    }
    
    // Normalize normals.
    for ( int i = 0; i < normals.size(); i++ )
        normals[i].normalize();
}


// Draws the model using openGL.
void Gmeshmodel:: draw_opengl ( void )
{
#ifdef GRAY_HAVE_OGL
    mat->draw_opengl();
    
    glmeshbuffer buffer ( vertices.size(),triFaces.size(),quadFaces.size() );
    
    for ( int i = 0; i < vertices.size(); i++ )
        buffer.add_vertex ( vertices[i],normals[i] );
    
    for ( int i = 0; i < triFaces.size(); i++ )
        buffer.add_triangle ( triFaces[i].vi[0],triFaces[i].vi[1],triFaces[i].vi[2] );
    
    for ( int i = 0; i < quadFaces.size(); i++ )
        buffer.add_quad ( quadFaces[i].vi[0],quadFaces[i].vi[1],
                         quadFaces[i].vi[2],quadFaces[i].vi[3] );
    
    buffer.draw();
#endif // GRAY_HAVE_OGL
}


// Loads a mesh model from a ply file.
void Gmeshmodel::read_ply ( const std::string& filename, const double& coeff,
                           const double& DX, const double& DY, const double& DZ )
{
#ifdef GRAY_HAVE_PLY
    struct Vertex
    {
        float x,y,z; // The usual 3-space position of a vertex.
    };
    
    struct Face
    {
        int nverts; // Number of vertex indices in list.
        int *verts; // Vertex index list.
    };
    
    // Information needed to describe the user's data to the PLY routines.
    
    // List of the kinds of elements in the user's object.
    const char *elem_names[] =
    {
        "vertex", "face"
    };
    
    // List of property information for a vertex.
    PlyProperty vert_props[] =
    {
        { "x", PLY_FLOAT, PLY_FLOAT, offsetof ( Vertex,x ), 0, 0, 0, 0 },
        { "y", PLY_FLOAT, PLY_FLOAT, offsetof ( Vertex,y ), 0, 0, 0, 0 },
        { "z", PLY_FLOAT, PLY_FLOAT, offsetof ( Vertex,z ), 0, 0, 0, 0 },
    };
    
    // List of property information for a vertex.
    PlyProperty face_props[] =
    {
        {
            "vertex_indices", PLY_INT, PLY_INT, offsetof ( Face,verts ),
            1, PLY_INT, PLY_INT, offsetof ( Face,nverts )
        },
    };
    
    PlyFile *ply;
    int nelems;
    char **elist;
    int file_type;
    float version;
    int nprops;
    int num_elems;
    PlyProperty **plist;
    char *elem_name;
    
    char fname[500];
    int length = filename.copy ( fname, 500 );
    fname[length] = '\0';
    
    // Open a PLY file for reading.
    ply = ply_open_for_reading ( fname, &nelems, &elist, &file_type, &version );
    
    printf("Chargement du fichier : %s \n", fname);
    
    assert(ply != NULL);
    
    // Go through each kind of element that we learned is in the file
    // and read them.
    for (int i = 0; i < nelems; i++ )
    {
        // Get the description of the first element.
        elem_name = elist[i];
        plist = ply_get_element_description ( ply, elem_name, &num_elems, &nprops );
        
        // If we're on vertex elements, read them in.
        if ( equal_strings ( "vertex", elem_name ) )
        {
            // Default values for the wrapping box.
            min = Point ( 1e90,1e90,1e90 );
            max = Point ( -1e90,-1e90,-1e90 );
            
            // Create a vertex list to hold all the vertices.
            vertices.resize ( num_elems );
            adjEdges.resize ( num_elems );
            adjFaces.resize ( num_elems );
            
            // Set up for getting vertex elements.
            ply_get_property ( ply, elem_name, &vert_props[0] ); // x
            ply_get_property ( ply, elem_name, &vert_props[1] ); // y
            ply_get_property ( ply, elem_name, &vert_props[2] ); // z
            
            // Grab all the vertex elements.
            for (int j = 0; j < num_elems; j++ )
            {
                Vertex v;
                ply_get_element ( ply, ( void* ) &v );
                
                vertices[j].X = v.x*coeff + DX;
                vertices[j].Y = v.y*coeff + DY;
                vertices[j].Z = v.z*coeff + DZ;
                adjEdges[j] = 0;
                adjFaces[j] = 0;
                min.X = ( vertices[j].X < min.X ) ? vertices[j].X : min.X;
                min.Y = ( vertices[j].Y < min.Y ) ? vertices[j].Y : min.Y;
                min.Z = ( vertices[j].Z < min.Z ) ? vertices[j].Z : min.Z;
                max.X = ( vertices[j].X > max.X ) ? vertices[j].X : max.X;
                max.Y = ( vertices[j].Y > max.Y ) ? vertices[j].Y : max.Y;
                max.Z = ( vertices[j].Z > max.Z ) ? vertices[j].Z : max.Z;
            }
        }
        
        // If we're on face elements, read them in.
        if ( equal_strings ( "face", elem_name ) )
        {
            // Create a list to hold all the face elements.
            triFaces.reserve ( num_elems );
            quadFaces.reserve ( num_elems );
            
            // Set up for getting face elements.
            ply_get_property ( ply, elem_name, &face_props[0] );
            
            // Grab all the face elements.
            for (int j = 0; j < num_elems; j++ )
            {
                // Grab an element from the file.
                Face* f = ( Face* ) malloc ( sizeof ( Face ) );
                ply_get_element ( ply, ( void* ) f );
                
                if ( f->nverts == 3 )
                {
                    addTriangle ( f->verts[0], f->verts[1], f->verts[2] );
                }
                else if ( f->nverts == 4 )
                {
                    addQuad ( f->verts[0], f->verts[1], f->verts[2], f->verts[3] );
                }
                
                free ( f->verts );
                free ( f );
            }
        }
    }
    
    // Close the PLY file.
    ply_close ( ply );
#endif // GRAY_HAVE_PLY
}


void Gmeshmodel::mergeCloseVertices ( double eps )
{
    std::vector<AdjEdge*> newAdjEdges;
    std::vector<AdjFace*> newAdjFaces;
    
    newAdjEdges.resize ( vertices.size() );
    newAdjFaces.resize ( vertices.size() );
    
    std::vector<Point> newVertices;
    std::vector<Tri>  newTriFaces;
    std::vector<Quad> newQuadFaces;
    
    int* transTab = new int[vertices.size()];
    
    for ( int i = 0; i < vertices.size(); i++ )
        transTab[i] = i;
    
    int nbVertices = vertices.size();
    
    for ( int i = 0; i < vertices.size(); i++ )
    {
        bool merged = false;
        
        for ( int j = 0; j < i && ( !merged ); j++ )
        {
            double dist = 0.0;
            
            dist += ( vertices[i].X - vertices[j].X ) *
            ( vertices[i].X - vertices[j].X );
            
            dist += ( vertices[i].Y - vertices[j].Y ) *
            ( vertices[i].Y - vertices[j].Y );
            
            dist += ( vertices[i].Z - vertices[j].Z ) *
            ( vertices[i].Z - vertices[j].Z );
            
            dist = std::sqrt ( dist );
            
            if ( dist <= eps )
            {
                transTab[i] = transTab[j];
                merged = true;
                nbVertices--;
            }
        }
    }
    
    // Used to know how much to shift unique vertices IDs
    int* shiftTab = new int[vertices.size()];
    
    // Initialize newVertices
    int shiftNb = 0;
    
    for ( int i = 0; i < vertices.size(); i++ )
    {
        if ( transTab[i] == i )
        {
            newVertices.push_back ( vertices[i] );
            shiftTab[i] = shiftNb;
        }
        
        else
            shiftNb++;
    }
    
    // The correct ID for a old vertex is the one contained in 'transTab'.
    // However, these are shifted by the number of previously deleted vertices.
    
    // Initialize newTriFaces
    for ( int i = 0; i < triFaces.size(); i++ )
    {
        addTriangle ( transTab[triFaces[i].vi[0]]
                     - shiftTab[transTab[triFaces[i].vi[0]]],
                     transTab[triFaces[i].vi[1]]
                     - shiftTab[transTab[triFaces[i].vi[1]]],
                     transTab[triFaces[i].vi[2]]
                     - shiftTab[transTab[triFaces[i].vi[2]]],
                     newAdjEdges, newAdjFaces, newTriFaces );
    }
    
    // Initialize newQuadfaces
    for ( int i = 0; i < quadFaces.size(); i++ )
    {
        addQuad ( transTab[quadFaces[i].vi[0]]
                 - shiftTab[transTab[quadFaces[i].vi[0]]],
                 transTab[quadFaces[i].vi[1]]
                 - shiftTab[transTab[quadFaces[i].vi[1]]],
                 transTab[quadFaces[i].vi[2]]
                 - shiftTab[transTab[quadFaces[i].vi[2]]],
                 transTab[quadFaces[i].vi[3]]
                 - shiftTab[transTab[quadFaces[i].vi[3]]],
                 newAdjEdges, newAdjFaces, newQuadFaces );
    }
    
    // Apply changes to the current model
    vertices = newVertices;
    triFaces = newTriFaces;
    quadFaces = newQuadFaces;
    adjEdges = newAdjEdges;
    adjFaces = newAdjFaces;
    
    // Refresh 'oTree'
    buildOTree();
    
    delete[] shiftTab;
    delete[] transTab;
}


////////////////////////////////////////////////////////////////////////////////
// Catmul-Clark subdivision.


// Refines the model by using the Catmul-Clark subdivision.
void Gmeshmodel::subdivideCatmulClark ( const int& n )
{
    std::cout << "start CC on " << vertices.size() << " vertices and "
    << ( triFaces.size() + quadFaces.size() ) << " faces" << std::endl;
    
    for ( int i = 0; i < n; i++ )
    {
        int nbIniVert = vertices.size();
        std::vector<int> edgeVertexLabels;
        
        // Initialize VertexData vector structure
        maxCapacity = vertices.size() +
        3 * triFaces.size() + 4 * quadFaces.size();
        
        vData = new VertexData*[maxCapacity];
        
        for ( int a = 0; a < maxCapacity; a++ )
            vData[a] = 0;
        
        std::vector<AdjEdge*> newAdjEdges = std::vector<AdjEdge*> ( vertices.size() );
        std::vector<AdjFace*> newAdjFaces = std::vector<AdjFace*> ( vertices.size() );
        std::vector<Quad> newQuadFaces = std::vector<Quad> ( 0 );
        newQuadFaces.reserve ( 4 * quadFaces.size() );
        vertices.reserve ( maxCapacity );
        
        /*
         1st pass
         - Add Face Vertices by dividing current faces.
         - Add Edge Vertices with incorrect position (which will be rectified
         by the 2nd pass).
         */
        firstCCPass ( edgeVertexLabels, newAdjEdges, newAdjFaces, newQuadFaces );
        
        /*
         2nd pass
         - Adjust Edge Vertices position.
         */
        secondCCPass ( edgeVertexLabels, newAdjEdges, newAdjFaces );
        
        /*
         3rd pass
         - Adjust Inital Vertices position.
         */
        thirdCCPass ( nbIniVert, newAdjEdges, newAdjFaces );
        
        // Delete old data.
        freeData();
        
        // Set new data.
        adjEdges = newAdjEdges;
        adjFaces = newAdjFaces;
        quadFaces = newQuadFaces;
        
        triFaces.clear();
        
        std::cout << "after step " << ( i+1 ) << " : " << vertices.size()
        << " vertices and " << ( triFaces.size() + quadFaces.size() )
        << " faces" << std::endl;
    }
    
    // Refresh 'oTree'
    buildOTree();
}


void Gmeshmodel::firstCCPass ( std::vector<int>& edgeVertexLabels,
                              std::vector<AdjEdge*>& newAdjEdges,
                              std::vector<AdjFace*>& newAdjFaces,
                              std::vector<Quad>& newQuadFaces )
{
    // Create a map used to mark edges that have already been splitted.
    // If the pair [(a, b), c] is found (with a < b), it means that the
    // edge (a, b) is already cut by c.
    // Else, it means that the edge (a, b) has not been cut yet.
    std::map< std::pair<int, int>, int > splittedEdges;
    
    // Quad Faces
    // Browse quad faces and split them
    std::vector<Quad>::iterator iQuad;
    
    for ( iQuad = quadFaces.begin(); iQuad < quadFaces.end(); iQuad++ )
        splitFace ( iQuad->vi[0], iQuad->vi[1], iQuad->vi[2], iQuad->vi[3],
                   splittedEdges, edgeVertexLabels,
                   newAdjEdges, newAdjFaces, newQuadFaces );
    
    // Tri Faces
    // Browse TriFaces and split them
    std::vector<Tri>::iterator iTri;
    
    for ( iTri = triFaces.begin(); iTri < triFaces.end(); iTri++ )
        splitFace ( iTri->vi[0], iTri->vi[1], iTri->vi[2],
                   splittedEdges, edgeVertexLabels,
                   newAdjEdges, newAdjFaces, newQuadFaces );
    
    // Empty 'splittedEdges' map
    splittedEdges.clear();
}


void Gmeshmodel::secondCCPass ( std::vector<int>& edgeVertexLabels,
                               std::vector<AdjEdge*>& newAdjEdges,
                               std::vector<AdjFace*>& newAdjFaces )
{
    // Adjust Edge Vertices position of vertices that have to be updated.
    std::vector<int>::iterator i;
    
    for ( i = edgeVertexLabels.begin(); i < edgeVertexLabels.end(); i++ )
    {
        refreshEdgeVertex ( *i, newAdjEdges, newAdjFaces );
    }
}


void Gmeshmodel::thirdCCPass ( int nbIniVert,
                              std::vector<AdjEdge*>& newAdjEdges,
                              std::vector<AdjFace*>& newAdjFaces )
{
    // Adjust Initial Vertices position of vertices that have to be updated.
    std::vector<Point>::iterator i;
    
    for ( int j = 0; j < nbIniVert; i++, j++ )
    {
        refreshInitialVertex ( j, newAdjEdges, newAdjFaces );
    }
}


void Gmeshmodel::refreshEdgeVertex ( int vID,
                                    std::vector<AdjEdge*>& newAdjEdges,
                                    std::vector<AdjFace*>& newAdjFaces )
{
    // Check if the vertex 'vID' has to be updated. If it is, it means that
    // all needed data has been collected.
    if ( vData[vID] == 0 || // No data collected or already updated
        vData[vID]->nF != vData[vID]->nbIniAdjFaces ||
        vData[vID]->nE != 2 )
        return;
    
    Point Q = vData[vID]->vF * ( 1.0 / ( ( double ) vData[vID]->nF ) );
    Point R = vData[vID]->vE * ( 1.0 / ( ( double ) vData[vID]->nE ) );
    
    vertices[vID] = ( Q + R ) * 0.5;
    
    // Delete the VertexData (no need to keep it).
    delete vData[vID];
    vData[vID] = 0;
}


void Gmeshmodel::refreshInitialVertex ( int vID,
                                       std::vector<AdjEdge*>& newAdjEdges,
                                       std::vector<AdjFace*>& newAdjFaces )
{
    // Check if the vertex 'vID' has to be updated. If it is, it means that
    // all needed data has been collected.
    if ( vData[vID] == 0 || // No data collected or already updated
        vData[vID]->nF != adjFaces[vID]->size ||
        vData[vID]->nE != adjEdges[vID]->size )
        return;
    
    Point Q = vData[vID]->vF * ( 1.0 / ( ( double ) vData[vID]->nF ) );
    Point R = vData[vID]->vE * ( 1.0 / ( ( double ) vData[vID]->nE ) );
    Point S = vertices[vID];
    double  N = vData[vID]->nE;
    
    vertices[vID] = ( Q + ( R * 2.0 ) + ( S * ( N - 3 ) ) ) * ( 1.0 / N );
    
    // Delete the VertexData (no need to keep it).
    delete vData[vID];
    vData[vID] = 0;
}


int Gmeshmodel::splitEdge ( int aLabel, int bLabel,
                           std::map< std::pair<int, int>, int >& splittedEdges,
                           std::vector<int>& edgeVertexLabels,
                           std::vector<AdjEdge*>& newAdjEdges,
                           std::vector<AdjFace*>& newAdjFaces )
{
    // Check if edge (a, b) has already been cut.
    // We return the mid-point label if it's the case.
    std::pair<int, int> tmp;
    std::map< std::pair<int, int>, int >::iterator it;
    
    // Search for ([a, b], c) or ([b, a], c) pair (first ID is less or
    // equal to the second one).
    if ( aLabel <= bLabel )
        tmp = std::make_pair ( aLabel, bLabel );
    
    else
        tmp = std::make_pair ( bLabel, aLabel );
    
    it = splittedEdges.find ( tmp );
    
    // Found pair
    if ( it != splittedEdges.end() )
        return ( *it ).second;
    
    
    // Create and add the mid-point
    // Create the mid-point
    Point c = ( vertices[aLabel] + vertices[bLabel] ) * 0.5;
    
    // Add it to 'vertices' vector
    int cLabel = vertices.size();
    vertices.push_back ( c );
    newAdjEdges.push_back ( 0 ); // Add an entry in 'newAdjEdges' vector for it.
    newAdjFaces.push_back ( 0 ); // Add an entry in 'newAdjFaces' vector for it.
    
    // Add its label to 'edgeVertexLabels' vector
    edgeVertexLabels.push_back ( cLabel );
    
    // Create and add an entry in 'vData'
    if ( vData[cLabel] == 0 )
        vData[cLabel] = new VertexData();
    
    vData[cLabel]->addE ( vertices[aLabel] );
    vData[cLabel]->addE ( vertices[bLabel] );
    
    // Computing 'nbIniAdjFaces' of the added VertexData
    typedef std::set<int, std::less<int> > intSet; // used for union operation
    AdjFace* p;
    
    // Build sets for 'aLabel' adjacent faces
    intSet aQuadAdjFaces;
    intSet aTriAdjFaces;
    p = adjFaces[aLabel];
    
    while ( p != 0 )
    {
        if ( p->nbVertices == 3 )
            aTriAdjFaces.insert ( p->index );
        
        else // p->nbVertices == 4
            aQuadAdjFaces.insert ( p->index );
        
        p = p->next;
    }
    
    // Build sets for 'bLabel' adjacent faces
    intSet bQuadAdjFaces;
    intSet bTriAdjFaces;
    p = adjFaces[bLabel];
    
    while ( p != 0 )
    {
        if ( p->nbVertices == 3 )
            bTriAdjFaces.insert ( p->index );
        
        else // p->nbVertices == 4
            bQuadAdjFaces.insert ( p->index );
        
        p = p->next;
    }
    
    // Compute number of common faces
    int val = 0;
    intSet tmp3;
    
    set_intersection ( aTriAdjFaces.begin(), aTriAdjFaces.end(),
                      bTriAdjFaces.begin(), bTriAdjFaces.end(),
                      std::insert_iterator<intSet> ( tmp3, tmp3.begin() ) );
    val += tmp3.size();
    
    tmp3.clear();
    set_intersection ( aQuadAdjFaces.begin(), aQuadAdjFaces.end(),
                      bQuadAdjFaces.begin(), bQuadAdjFaces.end(),
                      std::insert_iterator<intSet> ( tmp3, tmp3.begin() ) );
    val += tmp3.size();
    
    vData[cLabel]->nbIniAdjFaces = val;
    
    // Mark the edge (a, b) as splitted
    std::pair<int, int> tmp2;
    
    // The edge to insert has to be defined with the minimal ID at first.
    if ( aLabel <= bLabel )
        tmp2 = std::make_pair ( aLabel, bLabel );
    
    else
        tmp2 = std::make_pair ( bLabel, aLabel );
    
    splittedEdges[tmp2] = cLabel;
    
    // Update 'vData' for 'a' and 'b' vertices
    if ( vData[aLabel] == 0 )
        vData[aLabel] = new VertexData();
    
    if ( vData[bLabel] == 0 )
        vData[bLabel] = new VertexData();
    
    vData[aLabel]->addE ( vertices[cLabel] );
    vData[bLabel]->addE ( vertices[cLabel] );
    
    return cLabel;
}


void Gmeshmodel::splitFace ( int aLabel, int bLabel,
                            int cLabel, int dLabel,
                            std::map<std::pair<int, int>, int >& splittedEdges,
                            std::vector<int>& edgeVertexLabels,
                            std::vector<AdjEdge*>& newAdjEdges,
                            std::vector<AdjFace*>& newAdjFaces,
                            std::vector<Quad>& newQuadFaces )
{
    // Compute and add face point for this face
    int fLabel = vertices.size();
    Point fVertex = ( vertices[aLabel] + vertices[bLabel] +
                     vertices[cLabel] + vertices[dLabel] )
    * 0.25;
    vertices.push_back ( fVertex );
    newAdjEdges.push_back ( 0 ); // Add an entry in 'newAdjEdges' vector for it.
    newAdjFaces.push_back ( 0 ); // Add an entry in 'newAdjFaces' vector for it.
    
    // - Split edges of the face
    // - Create 'vData' entries for new vertices
    int e1Label = splitEdge ( aLabel, bLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    int e2Label = splitEdge ( bLabel, cLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    int e3Label = splitEdge ( cLabel, dLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    int e4Label = splitEdge ( dLabel, aLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    
    // Add new faces
    addQuad ( aLabel, e1Label, fLabel, e4Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    addQuad ( bLabel, e2Label, fLabel, e1Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    addQuad ( cLabel, e3Label, fLabel, e2Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    addQuad ( dLabel, e4Label, fLabel, e3Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    
    // Update 'vData' entries according to the face point computed
    if ( vData[aLabel] == 0 )
        vData[aLabel] = new VertexData();
    
    if ( vData[bLabel] == 0 )
        vData[bLabel] = new VertexData();
    
    if ( vData[cLabel] == 0 )
        vData[cLabel] = new VertexData();
    
    if ( vData[dLabel] == 0 )
        vData[dLabel] = new VertexData();
    
    vData[aLabel]->addF ( fVertex );
    vData[bLabel]->addF ( fVertex );
    vData[cLabel]->addF ( fVertex );
    vData[dLabel]->addF ( fVertex );
    
    if ( vData[e1Label] == 0 )
        vData[aLabel] = new VertexData();
    
    if ( vData[e2Label] == 0 )
        vData[bLabel] = new VertexData();
    
    if ( vData[e3Label] == 0 )
        vData[cLabel] = new VertexData();
    
    if ( vData[e4Label] == 0 )
        vData[dLabel] = new VertexData();
    
    vData[e1Label]->addF ( fVertex );
    vData[e2Label]->addF ( fVertex );
    vData[e3Label]->addF ( fVertex );
    vData[e4Label]->addF ( fVertex );
    //
    
    // Update vertices positions if possible in order to consume less memory
    refreshInitialVertex ( aLabel, newAdjEdges, newAdjFaces );
    refreshInitialVertex ( bLabel, newAdjEdges, newAdjFaces );
    refreshInitialVertex ( cLabel, newAdjEdges, newAdjFaces );
    refreshInitialVertex ( dLabel, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e1Label, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e2Label, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e3Label, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e4Label, newAdjEdges, newAdjFaces );
}


void Gmeshmodel::splitFace ( int aLabel, int bLabel, int cLabel,
                            std::map< std::pair<int, int>, int >& splittedEdges,
                            std::vector<int>& edgeVertexLabels,
                            std::vector<AdjEdge*>& newAdjEdges,
                            std::vector<AdjFace*>& newAdjFaces,
                            std::vector<Quad>& newQuadFaces )
{
    // Compute and add face point for this face
    int fLabel = vertices.size();
    Point fVertex = ( vertices[aLabel] + vertices[bLabel] + vertices[cLabel] )
    * ( 1.0 / 3.0 );
    
    vertices.push_back ( fVertex );
    newAdjEdges.push_back ( 0 ); // Add an entry in 'newAdjEdges' vector for it.
    newAdjFaces.push_back ( 0 ); // Add an entry in 'newAdjFaces' vector for it.
    
    // - Split edges of the face
    // - Create 'vData' entries for new vertices
    int e1Label = splitEdge ( aLabel, bLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    int e2Label = splitEdge ( bLabel, cLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    int e3Label = splitEdge ( cLabel, aLabel, splittedEdges,
                             edgeVertexLabels, newAdjEdges, newAdjFaces );
    
    // Add new faces
    addQuad ( fLabel, e3Label, aLabel, e1Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    addQuad ( fLabel, e1Label, bLabel, e2Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    addQuad ( fLabel, e2Label, cLabel, e3Label,
             newAdjEdges, newAdjFaces, newQuadFaces );
    
    // Update 'vData' entries according to the face point computed
    if ( vData[aLabel] == 0 )
        vData[aLabel] = new VertexData();
    
    if ( vData[bLabel] == 0 )
        vData[bLabel] = new VertexData();
    
    if ( vData[cLabel] == 0 )
        vData[cLabel] = new VertexData();
    
    vData[aLabel]->addF ( fVertex );
    vData[bLabel]->addF ( fVertex );
    vData[cLabel]->addF ( fVertex );
    
    if ( vData[e1Label] == 0 )
        vData[aLabel] = new VertexData();
    
    if ( vData[e2Label] == 0 )
        vData[bLabel] = new VertexData();
    
    if ( vData[e3Label] == 0 )
        vData[cLabel] = new VertexData();
    
    vData[e1Label]->addF ( fVertex );
    vData[e2Label]->addF ( fVertex );
    vData[e3Label]->addF ( fVertex );
    
    // Update vertices positions if possible in order to consume less memory
    refreshInitialVertex ( aLabel, newAdjEdges, newAdjFaces );
    refreshInitialVertex ( bLabel, newAdjEdges, newAdjFaces );
    refreshInitialVertex ( cLabel, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e1Label, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e2Label, newAdjEdges, newAdjFaces );
    refreshEdgeVertex ( e3Label, newAdjEdges, newAdjFaces );
}


////////////////////////////////////////////////////////////////////////////////
// sqrt3 subdivision.


// Refines the model by using the sqrt3 subdivision.
void Gmeshmodel::subdivideSqrt3 ( const int& nbStep )
{
    if ( !quadFaces.empty() )
        throw std::logic_error ( "sqrt3 subdivision can only use triangles" );
    
    std::cout << "start sqrt3 on " << vertices.size() << " vertices and "
    << triFaces.size() << " faces" << std::endl;
    
    bool step1 = true;
    int nbBorders = 0;
    
    for ( int s = 0; s < nbStep; s++ )
    {
        int nbVertices = vertices.size();
        int nbFaces = triFaces.size();
        
        // Work on new vectors to be able to access old values.
        // (1 vertex is created by regular triangles -> (nbFaces-nbBorders) )
        // (2 vertices are created by border triangles -> (2*nbBorders) )
        int nb = nbVertices + nbFaces + nbBorders;
        std::vector<Point> newVertices = std::vector<Point> ( nb );
        std::vector<AdjEdge*> newAdjEdges = std::vector<AdjEdge*> ( nb );
        std::vector<AdjFace*> newAdjFaces = std::vector<AdjFace*> ( nb );
        std::vector<Tri> newTriFaces;
        newTriFaces.reserve ( 3*nbFaces );
        
        std::vector<Tri> borders;
        
        // Smoothing existing vertices.
        for ( int i = 0; i < nbVertices; i++ )
            smooth ( i, step1, nbVertices, newVertices );
        
        // Splitting (+ flipping) existing triangles.
        for ( int i = 0; i < nbFaces-nbBorders; i++ )
            split ( i, step1, nbVertices,
                   newVertices, newAdjEdges ,newAdjFaces, newTriFaces, borders );
        
        if ( step1 )
        {
            // Step 1 : add border triangles after regular triangles.
            std::vector<Tri>::iterator it;
            
            for ( it = borders.begin(); it < borders.end(); it++ )
            {
                addTriangle ( it->vi[0], it->vi[1], it->vi[2],
                             newAdjEdges, newAdjFaces, newTriFaces );
            }
        }
        else if ( borders.size() > 0 )
        {
            throw std::logic_error (
                                    "Gmeshmodel::subdivideSqrt3 - step 2 : borders should be empty" );
        }
        else
        {
            // Step 2 : handle border triangles created in step 1.
            for ( int i = nbFaces-nbBorders; i < nbFaces; i++ )
            {
                borderStep2 ( i, nbVertices, nbBorders,
                             newVertices, newAdjEdges, newAdjFaces, newTriFaces );
            }
        }
        
        // Delete old data.
        freeData();
        
        // Set new data.
        vertices = newVertices;
        adjEdges = newAdjEdges;
        adjFaces = newAdjFaces;
        triFaces = newTriFaces;
        
        // Prepare next step.
        step1 = !step1;
        nbBorders = borders.size();
        
        std::cout << "after step " << ( s+1 ) << " : " << vertices.size()
        << " vertices and " << triFaces.size() << " faces" << std::endl;
    }
    
    // Refresh 'oTree'
    buildOTree();
}


// Smoothing part of sqrt3.
void Gmeshmodel::smooth ( const int& index, const bool& step1, const int& oldSize,
                         std::vector<Point> &newVertices )
{
    Point sum = Point ( 0,0,0 );
    int valence = 0;
    bool border = false;
    
    // Get needed info from neighbouring vertices.
    AdjEdge* edge = adjEdges[index];
    
    while ( edge != 0 )
    {
        valence++;
        sum = sum + vertices[edge->neighbour];
        border = border || edge->border;
        edge = edge->next;
    }
    
    if ( !border )
    {
        // Not a border : smooth vertex.
        initAlphaBeta ( valence );
        newVertices[index] = ( vertices[index] * alphas[valence] )
        + ( sum * betas[valence] );
    }
    else if ( step1 )
    {
        // Border - step 1 : keep vertex.
        newVertices[index] = vertices[index];
    }
    else
    {
        // Border - step 2 : is handled in method borderStep2.
    }
}


// Splitting (and flipping) part of sqrt3.
void Gmeshmodel::split ( const int& index, const bool& step1, const int& oldSize,
                        std::vector<Point> &newVertices,
                        std::vector<AdjEdge*> &newAdjEdges,
                        std::vector<AdjFace*> &newAdjFaces,
                        std::vector<Tri> &newTriFaces,
                        std::vector<Tri> &borders )
{
    Tri tri = triFaces[index];
    
    // Indexes of adjacent triangles.
    int adj[3];
    adj[0] = searchAdjTriangle ( tri.vi[0], tri.vi[1], index );
    adj[1] = searchAdjTriangle ( tri.vi[1], tri.vi[2], index );
    adj[2] = searchAdjTriangle ( tri.vi[2], tri.vi[0], index );
    
    if ( ( !step1 ) && ( ( adj[0] < 0 ) || ( adj[1] < 0 ) || ( adj[2] < 0 ) ) )
    {
        // Border - step 2 : is handled in method borderStep2.
        return;
    }
    
    // New vertex on center of triangle.
    newVertices[oldSize + index] = ( vertices[tri.vi[0]]
                                    + vertices[tri.vi[1]]
                                    + vertices[tri.vi[2]] ) * ( 1./3 );
    
    // Handle each edge of triangle.
    for ( int j = 0; j < 3; j++ )
    {
        if ( adj[j] < 0 )
        {
            // Border - step 1 : create 1 new border triangle.
            borders.push_back ( Tri ( oldSize + index, tri.vi[j], tri.vi[ ( j+1 ) %3] ) );
        }
        else if ( adj[j] < index )
        {
            // Do it only once by pair of adjacent triangles to avoid duplicates.
            // Create 2 new triangles.
            addTriangle ( oldSize + index, tri.vi[j], oldSize + adj[j],
                         newAdjEdges, newAdjFaces, newTriFaces );
            addTriangle ( oldSize + adj[j], tri.vi[ ( j+1 ) %3], oldSize + index,
                         newAdjEdges, newAdjFaces, newTriFaces );
            // As border triangles are added at the end of the triFaces vector,
            // their indexes (adj[j]) will be greater than regular triangles.
            // Then, we are sure the new vertex (oldSize + adj[j]) exists.
        }
    }
}


// Border triangles must be manage in a special way every other step in sqrt3.
void Gmeshmodel::borderStep2 ( const int& index, const int& oldSize,
                              const int& nbBorders,
                              std::vector<Point> &newVertices,
                              std::vector<AdjEdge*> &newAdjEdges,
                              std::vector<AdjFace*> &newAdjFaces,
                              std::vector<Tri> &newTriFaces )
{
    Tri tri = triFaces[index];
    
    // Indexes of adjacent triangles.
    int adj[3];
    adj[0] = searchAdjTriangle ( tri.vi[0], tri.vi[1], index );
    adj[1] = searchAdjTriangle ( tri.vi[1], tri.vi[2], index );
    adj[2] = searchAdjTriangle ( tri.vi[2], tri.vi[0], index );
    
    //               c
    //               /\
    //         t_ca /  \ t_bc
    //             /    \
    //            /      \
    // /________\/________\/________\
    // p         a  x  y  b         q
    
    // Find the border ab.
    int a, b, c; // Indexes of vertices a, b and b.
    int t_bc, t_ca; // Indexes of adjacent triangles to bc and ca.
    
    for ( int i = 0; i < 3; i++ )
    {
        if ( adj[i] < 0 )
        {
            a = tri.vi[i];
            b = tri.vi[ ( i+1 ) %3];
            c = tri.vi[ ( i+2 ) %3];
            t_bc = adj[ ( i+1 ) %3];
            t_ca = adj[ ( i+2 ) %3];
        }
    }
    
    // Find next border vertices p and q.
    int p = searchNextBorderVertex ( b, a ); // b -> a -> p
    int q = searchNextBorderVertex ( a, b ); // a -> b -> q
    
    // Indexes of new vertices x and y.
    int x = oldSize + index; // Same index as regular triangle.
    int y = oldSize + nbBorders + index; // Additional index.
    
    // Smooth existing vertices a and b.
    // a = (1/27)(4*p + 19*a + 4*b)
    newVertices[a] = ( vertices[a]*19 + ( vertices[p]+vertices[b] ) *4 ) * ( 1./27 );
    // b = (1/27)(4*a + 19*b + 4*q)
    newVertices[b] = ( vertices[b]*19 + ( vertices[a]+vertices[q] ) *4 ) * ( 1./27 );
    
    // Create new vertices x and y.
    // x = (1/27)(p + 16*a + 10*b)
    newVertices[x] = ( vertices[p] + vertices[a]*16 + vertices[b]*10 ) * ( 1./27 );
    // y = (1/27)(10*a + 16*b + q)
    newVertices[y] = ( vertices[a]*10 + vertices[b]*16 + vertices[q] ) * ( 1./27 );
    
    // Create new triangles.
    addTriangle ( a, x, oldSize + t_ca, newAdjEdges, newAdjFaces, newTriFaces );
    addTriangle ( x, c, oldSize + t_ca, newAdjEdges, newAdjFaces, newTriFaces );
    addTriangle ( x, y, c, newAdjEdges, newAdjFaces, newTriFaces );
    addTriangle ( c, y, oldSize + t_bc, newAdjEdges, newAdjFaces, newTriFaces );
    addTriangle ( y, b, oldSize + t_bc, newAdjEdges, newAdjFaces, newTriFaces );
}


// Initialize the alphas/betas coefficients of sqrt3.
void Gmeshmodel::initAlphaBeta ( const int& valence )
{
    for ( int n = alphas.size(); n <= valence; n++ )
    {
        if ( n == 0 )
        {
            alphas.push_back ( 1 );
            betas.push_back ( 0 );
        }
        else
        {
            double alpha = ( 4 - 2*cos ( 2*PI/n ) ) / 9;
            alphas.push_back ( 1 - alpha );
            betas.push_back ( alpha/n );
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
// Handling data vectors.


// Searches a triangle adjacent to an edge.
int Gmeshmodel::searchAdjTriangle ( const int& a, const int& b,
                                   const int& currentTriangle )
{
    AdjFace* face = adjFaces[a];
    
    while ( face != 0 )
    {
        if ( ( face->nbVertices == 3 ) and ( face->index != currentTriangle )
            and ( triFaces[face->index].vi[0] == b
                 or triFaces[face->index].vi[1] == b
                 or triFaces[face->index].vi[2] == b ) )
        {
            return face->index;
        }
        
        face = face->next;
    }
    
    return -1;
}


// Searches the next border edge.
int Gmeshmodel::searchNextBorderVertex ( const int& a, const int& b )
{
    AdjEdge* edge = adjEdges[b];
    
    while ( edge != 0 )
    {
        if ( edge->border && ( edge->neighbour != a ) )
        {
            return edge->neighbour;
        }
        
        edge = edge->next;
    }
    
    return b;
}


// Adds a triangle to the model.
void Gmeshmodel::addTriangle ( const int& a, const int& b, const int& c )
{
    addTriangle ( a, b, c, adjEdges, adjFaces, triFaces );
}


// Adds a triangle to the model.
void Gmeshmodel::addTriangle ( const int& a, const int& b, const int& c,
                              std::vector<AdjEdge*> &newAdjEdges,
                              std::vector<AdjFace*> &newAdjFaces,
                              std::vector<Tri> &newTriFaces )
{
    if ( ( a == b ) || ( a == c ) || ( b == c ) )
    {
        //    std::cout << "avoid triangle " << a << "-" << b << "-" << c << std::endl;
        return;
    }
    
    addEdge ( a, b, newAdjEdges );
    addEdge ( b, c, newAdjEdges );
    addEdge ( c, a, newAdjEdges );
    newTriFaces.push_back ( Tri ( a, b, c ) );
    newAdjFaces[a] = new AdjFace ( newTriFaces.size()-1, newAdjFaces[a] );
    newAdjFaces[b] = new AdjFace ( newTriFaces.size()-1, newAdjFaces[b] );
    newAdjFaces[c] = new AdjFace ( newTriFaces.size()-1, newAdjFaces[c] );
}


// Adds a quad to the model.
void Gmeshmodel::addQuad ( const int& a, const int& b,
                          const int& c, const int& d )
{
    addQuad ( a, b, c, d, adjEdges, adjFaces, quadFaces );
}


// Adds a quad to the model.
void Gmeshmodel::addQuad ( const int& a, const int& b,
                          const int& c, const int& d,
                          std::vector<AdjEdge*> &newAdjEdges,
                          std::vector<AdjFace*> &newAdjFaces,
                          std::vector<Quad> &newQuadFaces )
{
    addEdge ( a, b, newAdjEdges );
    addEdge ( b, c, newAdjEdges );
    addEdge ( c, d, newAdjEdges );
    addEdge ( d, a, newAdjEdges );
    newQuadFaces.push_back ( Quad ( a, b, c, d ) );
    newAdjFaces[a] = new AdjFace ( newQuadFaces.size()-1, newAdjFaces[a], 4 );
    newAdjFaces[b] = new AdjFace ( newQuadFaces.size()-1, newAdjFaces[b], 4 );
    newAdjFaces[c] = new AdjFace ( newQuadFaces.size()-1, newAdjFaces[c], 4 );
    newAdjFaces[d] = new AdjFace ( newQuadFaces.size()-1, newAdjFaces[d], 4 );
}


// Adds an edge to the model.
void Gmeshmodel::addEdge ( const int& a, const int& b,
                          std::vector<AdjEdge*> &newAdjEdges )
{
    AdjEdge* edge = newAdjEdges[a];
    
    while ( edge != 0 )
    {
        if ( edge->neighbour == b )
        {
            // Already exists -> not a border.
            edge->border = false;
            edge = newAdjEdges[b];
            
            // Search other side of the edge to also mark it as border.
            while ( edge != 0 )
            {
                if ( edge->neighbour == a )
                {
                    edge->border = false;
                    return;
                }
                
                edge = edge->next;
            }
            
            throw std::logic_error (
                                    "Gmeshmodel::addEdge - unable to find the other side of the edge" );
        }
        
        edge = edge->next;
    }
    
    // Doesn't exist -> create it.
    newAdjEdges[a] = new AdjEdge ( b, newAdjEdges[a] );
    newAdjEdges[b] = new AdjEdge ( a, newAdjEdges[b] );
}


// Frees pointers from the 'adjEdges' and 'adjFaces' data vectors.
void Gmeshmodel::freeData()
{
    // Free 'adjEdges' vector.
    std::vector<AdjEdge*>::iterator i;
    
    for ( i = adjEdges.begin(); i < adjEdges.end(); i++ )
    {
        AdjEdge* p = *i;
        
        while ( p != 0 )
        {
            AdjEdge* toDelete = p;
            p = p->next;
            
            delete toDelete;
            toDelete = 0;
        }
    }
    
    // Free 'adjFaces' vector.
    std::vector<AdjFace*>::iterator j;
    
    for ( j = adjFaces.begin(); j < adjFaces.end(); j++ )
    {
        AdjFace* p = *j;
        
        while ( p != 0 )
        {
            AdjFace* toDelete = p;
            p = p->next;
            delete toDelete;
            toDelete = 0;
        }
    }
    
    // Free 'vData' array.
    if ( vData != 0 )
    {
        for ( int a = 0; a < maxCapacity; a++ )
            if ( vData[a] != 0 )
            {
                delete vData[a];
                vData[a] = 0;
            }
        
        delete[] vData;
        vData = 0;
    }
}


////////////////////////////////////////////////////////////////////////////////
// OTree implementation

/**
 Building the OTree
 */
// Build the 'oTree'.
void Gmeshmodel::buildOTree()
{
    // Delete the current 'oTree' if it exists.
    if ( oTree != 0 )
    {
        delete oTree;
        oTree = 0;
    }
    
    // Create a new empty tree
    oTree = new Tree();
    oTree->min = min;
    oTree->max = max;
    
    // Add all vertices to the tree
    for ( int i = 0; i < vertices.size(); i++ )
        addVertexToTree ( 10, i, oTree );
}


// Add a vertex to the tree 't'.
void Gmeshmodel::addVertexToTree ( int ttl, int vID, Tree* t )
{
    // Current box is empty, we can associated the given vertex to it.
    if ( t->isEmpty || ttl == 0 || t->vID.size() > 1 )
    {
        t->vID.insert ( vID );
        t->isEmpty = false;
        t->oneVertex = true;
    }
    
    // Current box is not empty and has another vertex associated to it.
    else if ( !t->isEmpty && t->oneVertex )
    {
        // Add the new vertex to the correct sub-box
        for ( int i = 0; i < 8; i++ )
        {
            if ( isInInternalBoxI ( vertices[vID], t->min, t->max, i ) )
            {
                // Sub-Box 'i' is the good one but does not exist yet.
                // We have to create it.
                if ( t->childs[i] == 0 )
                {
                    t->childs[i] = new Tree();
                    getInternalBoxIMinMax ( t->min, t->max,
                                           t->childs[i]->min, t->childs[i]->max, i );
                }
                
                addVertexToTree ( ttl - 1, vID, t->childs[i] );
                break;
            }
        }
        
        // Add the current vertex to the correct sub-box.
        for ( int i = 0; i < 8; i++ )
        {
            if ( isInInternalBoxI ( vertices[* ( t->vID.begin() )], t->min, t->max, i ) )
            {
                // Sub-Box 'i' is the good one but does not exist yet.
                // We have to create it.
                if ( t->childs[i] == 0 )
                {
                    t->childs[i] = new Tree();
                    getInternalBoxIMinMax ( t->min, t->max,
                                           t->childs[i]->min, t->childs[i]->max, i );
                }
                
                addVertexToTree ( ttl - 1, * ( t->vID.begin() ), t->childs[i] );
                break;
            }
        }
        
        // Remove the current vertex from the main box.
        t->vID.clear();
        t->isEmpty = false;
        t->oneVertex = false;
    }
    
    // The current box is not empty and does not have any vertex associated
    // to it (it means that at least one of its sub-boxes is not empty).
    else if ( !t->isEmpty && !t->oneVertex )
    {
        for ( int i = 0; i < 8; i++ )
        {
            // Add the new vertex to the correct sub-box
            if ( isInInternalBoxI ( vertices[vID], t->min, t->max, i ) )
            {
                // Sub-Box 'i' is the good one but does not exist yet.
                // We have to create it.
                if ( t->childs[i] == 0 )
                {
                    t->childs[i] = new Tree();
                    getInternalBoxIMinMax ( t->min, t->max,
                                           t->childs[i]->min, t->childs[i]->max, i );
                }
                
                addVertexToTree ( ttl - 1, vID, t->childs[i] );
                break;
            }
        }
    }
}


// Check if the given point 'p' is within the sub-box 'boxNb' of the
// given box defined by 'min' and 'max' points.
bool Gmeshmodel::isInInternalBoxI ( Point p, Point min, Point max,
                                   int boxNb )
{
    Point tmpMin, tmpMax;
    getInternalBoxIMinMax ( min, max, tmpMin, tmpMax, boxNb );
    
    return isInBox ( p, tmpMin, tmpMax );
}


// Check if the given point 'p' is within the box defined by 'min' and 'max'
// points, with a tolerance of 'eps'.
bool Gmeshmodel::isInBox ( Point p, Point min, Point max, double eps )
{
    return ( p.X >= min.X - eps && p.X <= max.X + eps ) &&
    ( p.Y >= min.Y - eps && p.Y <= max.Y + eps ) &&
    ( p.Z >= min.Z - eps && p.Z <= max.Z + eps );
}


// Computes min/max points coordinates of internal box 'i', where
// the main box is defined by 'min' and 'max' points.
void Gmeshmodel::getInternalBoxIMinMax ( Point min, Point max,
                                        Point& iMin, Point& iMax,
                                        int boxNb )
{
    int a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;
    
    // Set correct coefficients according to the box chosen (see below)
    switch ( boxNb )
    {
        case 0:
            b = 1;
            c = 1;
            d = 1;
            break;
            
        case 1:
            a = 1;
            b = 1;
            c = 1;
            break;
            
        case 2:
            b = 1;
            d = 1;
            f = 1;
            break;
            
        case 3:
            a = 1;
            b = 1;
            f = 1;
            break;
            
        case 4:
            c = 1;
            d = 1;
            e = 1;
            break;
            
        case 5:
            a = 1;
            c = 1;
            e = 1;
            break;
            
        case 6:
            d = 1;
            e = 1;
            f = 1;
            break;
            
        case 7:
            a = 1;
            e = 1;
            f = 1;
            break;
    }
    
    // Set min/max vertices of the box according to coefficients.
    iMin.X = min.X + a * ( std::abs ( max.X - min.X ) / 2.0 );
    iMin.Y = min.Y + b * ( std::abs ( max.Y - min.Y ) / 2.0 );
    iMin.Z = min.Z + c * ( std::abs ( max.Z - min.Z ) / 2.0 );
    
    iMax.X = max.X - d * ( std::abs ( max.X - min.X ) / 2.0 );
    iMax.Y = max.Y - e * ( std::abs ( max.Y - min.Y ) / 2.0 );
    iMax.Z = max.Z - f * ( std::abs ( max.Z - min.Z ) / 2.0 );
}


/**
 Using the OTree
 */
// Add indexes of faces to test for ray/meshmodel intersection, according
// to the given box (which has to be built before). We supposed that there
// is intersection between 'r' and 'box'.
void Gmeshmodel::getIntersectedFaces ( int n, Ray r, Tree* box,
                                      std::set<int>& intTri,
                                      std::set<int>& intQuad )
{
    // No face associated to the current box.
    if ( box == 0 )
        return;
    
    // The box is associated to a vertex. We have to check adjacent faces
    // of this vertex.
    if ( box->oneVertex )
    {
        std::set<int>::iterator it;
        
        for ( it = box->vID.begin(); it != box->vID.end(); it++ )
        {
            AdjFace* p = adjFaces[*it];
            
            // Browse adjacent faces of the vertex
            while ( p != 0 )
            {
                // Current face is a 'Tri'
                if ( p->nbVertices == 3 )
                    intTri.insert ( p->index );
                
                // Current face is a 'Quad'
                if ( p->nbVertices == 4 )
                    intQuad.insert ( p->index );
                
                p = p->next;
            }
        }
    }
    
    // Check if there is an intersection with sub-boxes.
    // The test came after because we accept points of the current box if
    // we have to test it (see report for detail about 'oTree' optimization
    // issue).
    else if ( isHitByRay ( box, r ) )
        for ( int i = 0; i < 8; i++ )
            getIntersectedFaces ( n + 1, r, box->childs[i],
                                 intTri, intQuad );
}


// Check if a ray hit a given box (or nearby it, see comments
// in meshmodel.h or in source code below.
bool Gmeshmodel::isHitByRay ( Tree* box, Ray r )
{
    double tmp1, tmp2;
    return isHitByRay ( box, r, tmp1, tmp2 );
}


// Check if a ray hit a given box (or nearby it, see comments
// in meshmodel.h or in source code below. 'tnear' and 'tfar' ar used to
// to store 't' values for the first and second intersection.
bool Gmeshmodel::isHitByRay ( Tree* box, Ray r, double& tnear, double& tfar )
{
    if ( ( box == 0 ) || box->isEmpty )
        return false;
    
    // Adapted Smits_Mul Algorithm
    // Computes the 't' value of possible intersection point.
    tnear = -1e6;
    tfar =  1e6;
    
    {
        // multiply by the inverse instead of dividing
        double t1 = ( box->min.X - r.p.X ) * ( 1.0 / r.v.X );
        double t2 = ( box->max.X - r.p.X ) * ( 1.0 / r.v.X );
        
        //    t1 *= (-1);
        //    t2 *= (-1);
        
        if ( t1 > t2 )
        {
            double temp = t1;
            t1 = t2;
            t2 = temp;
        }
        
        if ( t1 > tnear )
            tnear = t1;
        
        if ( t2 < tfar )
            tfar = t2;
    }
    {
        double t1 = ( box->min.Y - r.p.Y ) * ( 1.0 / r.v.Y );
        double t2 = ( box->max.Y - r.p.Y ) * ( 1.0 / r.v.Y );
        
        //    t1 *= (-1);
        //    t2 *= (-1);
        
        if ( t1 > t2 )
        {
            double temp = t1;
            t1 = t2;
            t2 = temp;
        }
        
        if ( t1 > tnear )
            tnear = t1;
        
        if ( t2 < tfar )
            tfar = t2;
    }
    {
        double t1 = ( box->min.Z - r.p.Z ) * ( 1.0 / r.v.Z );
        double t2 = ( box->max.Z - r.p.Z ) * ( 1.0 / r.v.Z );
        
        //    t1 *= (-1);
        //    t2 *= (-1);
        
        if ( t1 > t2 )
        {
            double temp = t1;
            t1 = t2;
            t2 = temp;
        }
        
        if ( t1 > tnear )
            tnear = t1;
        
        if ( t2 < tfar )
            tfar = t2;
    }
    
    // There is some boxes that contains no vertex but contained some faces.
    // The solution used here is not the best one but works in most cases
    // with few modifications and few loss of performances.
    //
    // We consider a box bigger than the real one. It's used because when we
    // have an empty box which contains a face, there is no computation for that
    // because the box is considered empty. But faces that are in this box are,
    // with a very high probability, in a near box (considering the
    // construction algorithm of the tree). So, when a ray hits a nearby
    // box, we consider that the ray hit the little one even if it's false.
    // That allows to test faces of a box that is near empty boxes.
    //
    // Performances becomes worse because for each box, we test nearby boxes
    // as well. We chose a 'step' value that experimentally works with a few
    // loss of performances.
    double tmpMax;
    double step;
    step = std::sqrt ( ( box->max.X - box->min.X ) * ( box->max.X - box->min.X ) );
    
    tmpMax = std::sqrt ( ( box->max.Y - box->min.Y ) * ( box->max.Y - box->min.Y ) );
    
    if ( tmpMax > step )
        step = tmpMax;
    
    tmpMax = std::sqrt ( ( box->max.Z - box->min.Z ) * ( box->max.Z - box->min.Z ) );
    
    if ( tmpMax > step )
        step = tmpMax;
    
    step /= 1.75;
    
    // Check if the ray hit the box
    // 'tnear' is the 't' value of the first possible intersection
    // 'tfar'  is the 't' value of the last  possible intersection
    return ( isInBox ( r.p + ( r.v * tnear ),  box->min, box->max, step ) ||
            isInBox ( r.p + ( r.v * tfar ),   box->min, box->max, step ) );
}


// Prints out the given tree 't'.
void Gmeshmodel::printTree ( int tab, Tree* t )
{
    for ( int i = 0; i < tab; i++ )
        std::cout << "   ";
    
    if ( t == 0 )
    {
        std::cout << "Empty Box\n";
        return;
    }
    
    std::cout << "Box (" << t->min.X << ", " << t->min.Y << ", "
    << t->min.Z << ") -> (" << t->max.X << ", " << t->max.Y
    << ", " << t->max.Z << ")" << " - isEmpty: " << t->isEmpty
    << " - oneVertex: " << t->oneVertex;
    
    if ( !t->isEmpty )
    {
        if ( !t->oneVertex )
        {
            std::cout << "\n";
            
            for ( int i = 0; i < 8; i++ )
                printTree ( tab + 1, t->childs[i] );
        }
        else
        {
            int j = 0;
            std::set<int>::iterator it;
            
            for ( it = t->vID.begin(); it != t->vID.end(); it++ )
            {
                for ( int i = 0; i < tab + 1; i++ )
                    std::cout << "   ";
                
                std::cout << "v " << j++ << ":\t(" << vertices[*it].X
                << ", " << vertices[*it].Y << ", "
                << vertices[*it].Z << ")\n";
            }
        }
    }
}

void Gmeshmodel::getUVFromPoint ( Point point, double& u, double& v, double tilingU, double tilingV )
{
}


#undef PI

// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
