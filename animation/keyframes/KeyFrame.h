//
//  KeyFrame.h
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#ifndef __GRAY__KeyFrame__
#define __GRAY__KeyFrame__

#include <iostream>
#include <Transform.h>
#include "EasingFunction.h"
#include "scene.h"

class KeyFrame {
    
public:
    
    virtual KeyFrame* clone() = 0;
    
    virtual unsigned int getStart() const = 0;
    virtual unsigned int getDuration() const = 0;
    
    unsigned int getEnd() const {
        return getStart() + getDuration();
    }
    
    virtual double getInterpolationFractionForFrame(unsigned int frame) = 0;
    
    virtual void apply(const Gscene& scene, int frame) = 0;
};

#endif /* defined(__GRAY__KeyFrame__) */
