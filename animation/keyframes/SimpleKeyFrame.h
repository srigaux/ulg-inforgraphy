//
//  SimpleKeyFrame.h
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#ifndef __GRAY__SimpleKeyFrame__
#define __GRAY__SimpleKeyFrame__

#include <iostream>
#include <string>
#include "KeyFrame.h"
#include "ModelGroup.h"

class SimpleKeyFrame : public KeyFrame {
protected:
    unsigned int start;
    unsigned int duration;
    string groupName;
    Transform transform;
    const EasingFunction& easing;
    
public:
    
    SimpleKeyFrame(unsigned int start,
                   unsigned int duration,
                   const Group& group,
                   Transform transform,
                   const EasingFunction& easing):
        start(start),
        duration(duration),
        groupName(group.getName()),
        transform(transform),
        easing(easing)
        {}
    
    SimpleKeyFrame(const SimpleKeyFrame& k):
        start(k.start),
        duration(k.duration),
        groupName(k.groupName),
        transform(k.transform),
        easing(k.easing)
        {}
    
    KeyFrame* clone() { return new SimpleKeyFrame(*this); }
    
    unsigned int getStart() const { return start; }
    unsigned int getDuration() const { return duration; }

    string getGroupName() const { return groupName; }
    
    double getInterpolationFractionForFrame(unsigned int frame);
    
    Transform getTransform() const { return transform; }
    
    void apply(const Gscene& scene, int frame);
};

#endif /* defined(__GRAY__SimpleKeyFrame__) */
