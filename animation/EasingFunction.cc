//
//  EasingFunction.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//   https://github.com/warrenm/AHEasing
//

#include "EasingFunction.h"

float EasingFunction::ease(float current,
                           float start,
                           float duration) const
{
    
    float fraction = (current - start) / duration;
    
    switch (easing) {
        case EaseIn:
            return easeIn(fraction);
            
        case EaseOut:
            return easeOut(fraction);
            
        case EaseInOut:
            return easeInOut(fraction);
    }
}

#pragma mark - Linear

// y = x
float LinearEasingFunction::easeIn (float f) const {
	return f;
}

float LinearEasingFunction::easeOut(float f) const {
	return f;
}

float LinearEasingFunction::easeInOut(float f) const {
	return f;
}

#pragma mark - Quadratic

// y = x^2
float QuadraticEasingFunction::easeIn (float f) const {
	return f * f;
}

// y = -x^2 + 2x
float QuadraticEasingFunction::easeOut(float f) const {
	return -(f * (f - 2));
}

// y = (1/2)((2x)^2)             ; [0, 0.5)
// y = -(1/2)((2x-1)*(2x-3) - 1) ; [0.5, 1]
float QuadraticEasingFunction::easeInOut(float f) const {

	if(f < 0.5)
        return 2 * f * f;
    
    return (-2 * f * f) + (4 * f) - 1;
}

#pragma mark - Cubic

// y = x^3
float CubicEasingFunction::easeIn (float f) const {
	return f * f * f;
}

// y = (x - 1)^3 + 1
float CubicEasingFunction::easeOut(float f) const {
    f = f - 1;
	return f * f * f + 1;
}

// y = (1/2)((2x)^3)       ; [0, 0.5)
// y = (1/2)((2x-2)^3 + 2) ; [0.5, 1]
float CubicEasingFunction::easeInOut(float f) const {
    
	if(f < 0.5)
		return 4 * f * f * f;

    f = ((2 * f) - 2);
    
    return 0.5 * f * f * f + 1;
}

#pragma mark - Quartic

// y = x^4
float QuarticEasingFunction::easeIn (float f) const {
	return f * f * f * f;
}

// y = 1 - (x - 1)^4
float QuarticEasingFunction::easeOut(float f) const {
    float f2 = f - 1;
	return f2 * f2 * f2 * (1 - f) + 1;
}

// y = (1/2)((2x)^4)        ; [0, 0.5)
// y = -(1/2)((2x-2)^4 - 2) ; [0.5, 1]
float QuarticEasingFunction::easeInOut(float f) const {
    
	if(f < 0.5)
		return 8 * f * f * f * f;
    
    f = f - 1;
    
    return -8 * f * f * f * f + 1;
}

#pragma mark - Quintic

// y = x^5
float QuinticEasingFunction::easeIn (float f) const {
	return f * f * f * f * f;
}

// y = (x - 1)^5 + 1
float QuinticEasingFunction::easeOut(float f) const {

    f = f - 1;
    
	return f * f * f * f * f + 1;
}

// y = (1/2)((2x)^5)       ; [0, 0.5)
// y = (1/2)((2x-2)^5 + 2) ; [0.5, 1]
float QuinticEasingFunction::easeInOut(float f) const {
    
	if(f < 0.5)
		return 16 * f * f * f * f * f;
    
    f = ((2 * f) - 2);
    
    return  0.5 * f * f * f * f * f + 1;
}

#pragma mark - Sine

// quarter-cycle of sine wave
float SineEasingFunction::easeIn (float f) const {
    
	return sin((f - 1) * M_PI_2) + 1;
}

// quarter-cycle of sine wave
float SineEasingFunction::easeOut(float f) const {
    
    return sin(f * M_PI_2);
}

// half sine wave
float SineEasingFunction::easeInOut(float f) const {
    
	return 0.5 * (1 - cos(f * M_PI));
}

#pragma mark - Circular

// shifted quadrant IV of unit circle
float CircularEasingFunction::easeIn (float f) const {
    
	return 1 - sqrt(1 - (f * f));
}

// shifted quadrant II of unit circle
float CircularEasingFunction::easeOut(float f) const {
    
    return sqrt((2 - f) * f);
}

// y = (1/2)(1 - sqrt(1 - 4x^2))           ; [0, 0.5)
// y = (1/2)(sqrt(-(2x - 3)*(2x - 1)) + 1) ; [0.5, 1]
float CircularEasingFunction::easeInOut(float f) const {
    
    if(f < 0.5)
        return 0.5 * (1 - sqrt(1 - 4 * (f * f)));
    
    return 0.5 * (sqrt(-((2 * f) - 3) * ((2 * f) - 1)) + 1);
}

#pragma mark - Exponential

// y = 2^(10(x - 1))
float ExponentialEasingFunction::easeIn (float f) const {
    
	return (f == 0.0) ? f : pow(2, 10 * (f - 1));
}

// y = -2^(-10x) + 1
float ExponentialEasingFunction::easeOut(float f) const {
    
    return (f == 1.0) ? f : 1 - pow(2, -10 * f);
}


// y = (1/2)2^(10(2x - 1))         ; [0,0.5)
// y = -(1/2)*2^(-10(2x - 1))) + 1 ; [0.5,1]
float ExponentialEasingFunction::easeInOut(float f) const {
    
	if (f == 0.0 || f == 1.0)
        return f;
    
	if (f < 0.5)
        return 0.5 * pow(2, (20 * f) - 10);
	
    
	return -0.5 * pow(2, (-20 * f) + 10) + 1;
}


#pragma mark - Elastic

// y = sin(13pi/2*x)*pow(2, 10 * (x - 1))
float ElasticEasingFunction::easeIn (float f) const {
    
	return sin(13 * M_PI_2 * f) * pow(2, 10 * (f - 1));
}

// y = sin(-13pi/2*(x + 1))*pow(2, -10x) + 1
float ElasticEasingFunction::easeOut(float f) const {
    
    return sin(-13 * M_PI_2 * (f + 1)) * pow(2, -10 * f) + 1;
}

// y = (1/2)*sin(13pi/2*(2*x))*pow(2, 10 * ((2*x) - 1))      ; [0,0.5)
// y = (1/2)*(sin(-13pi/2*((2x-1)+1))*pow(2,-10(2*x-1)) + 2) ; [0.5, 1]
float ElasticEasingFunction::easeInOut(float f) const {
    
	if (f < 0.5)
        return 0.5 * sin(13 * M_PI_2 * (2 * f)) * pow(2, 10 * ((2 * f) - 1));
	
    
	return 0.5 * (sin(-13 * M_PI_2 * ((2 * f - 1) + 1)) * pow(2, -10 * (2 * f - 1)) + 2);
}


#pragma mark - Back

// y = x^3-x*sin(x*pi)
float BackEasingFunction::easeIn (float f) const {
    
	return f * f * f - f * sin(f * M_PI);
}

// y = 1-((1-x)^3-(1-x)*sin((1-x)*pi))
float BackEasingFunction::easeOut(float f) const {
    
    f = (1 - f);
	return 1 - (f * f * f - f * sin(f * M_PI));
}

// y = (1/2)*((2x)^3-(2x)*sin(2*x*pi))           ; [0, 0.5)
// y = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1) ; [0.5, 1]
float BackEasingFunction::easeInOut(float f) const {
    
	if (f < 0.5) {
        f = 2 * f;
		return 0.5 * (f * f * f - f * sin(f * M_PI));
    }
    else {
        f = (1 - (2*f - 1));
		return 0.5 * (1 - (f * f * f - f * sin(f * M_PI))) + 0.5;
    }
}

#pragma mark - Bounce

float BounceEasingFunction::easeIn(float f) const {
	return 1 - easeOut(1 - f);
}

float BounceEasingFunction::easeOut(float f) const {
	
    float f2 = f * f;
    
    if(f < 4/11.0)
        return (121 * f2)/16.0;
	
    if(f < 8/11.0)
        return (363/40.0 * f2) - (99/10.0 * f) + 17/5.0;
	
    if(f < 9/10.0)
        return (4356/361.0 * f2) - (35442/1805.0 * f) + 16061/1805.0;
	
    return (54/5.0 * f2) - (513/25.0 * f) + 268/25.0;
}

float BounceEasingFunction::easeInOut(float f) const {
	if(f < 0.5)
        return 0.5 * easeIn(f * 2);
	
    return 0.5 * easeOut(f * 2 - 1) + 0.5;
	
}