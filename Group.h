//
//  Group.h
//  GRAY
//
//  Created by Sébastien Rigaux on 15/04/14.
//
//

#ifndef __GRAY__Group__
#define __GRAY__Group__

#include <iostream>
#include <set>
#include <string>
#include <sstream>

#include "ray.h"
#include "models.h"
#include "scene.h"
class Gscene;

using namespace std;

typedef std::set<string> StringSet;

/**
 * Classe abstraite permettant de regrouper certains objets
 * pour �tre anim�s ensemble
 */
class Group {
    
protected:
	/**
	 * Le nom du groupe utilis� comme identifiant
	 */
    string name;

    /**
     * Ensemble des noms des sous-groupes
     */
    StringSet subGroupsNames;
    
    /**
     * Point d'ancrage du groupe
     */
    Point transformOrigin;
    
    /**
     * Indice permettant de g�n�rer des noms
     * diff�rents � chaque instantiation
     */
    static unsigned int groupIndex;
    
    /**
     * Retourne un nom unique
     */
    virtual string getDefaultName() const;

    /*
     * Fonction � red�finir.
     *
     * Applique la transformation 't' aux objets
     * de la sc�ne 'scene'
     */
    virtual void apply(const Gscene &scene, Transform t) = 0;
    
    /**
     * Ajoute un sous-groupe
     */
    void addSubGroup(const Group& group);

    /**
     * Ajoute un sous-�l�ment au groupe
     */
    void add(const Group& group) { addSubGroup(group); }
    
    /**
     * Initialise un 'Group' nom� 'name'
     * et dont le point d'ancrage est 'transformOrigin'
     */
    Group(string name, Point transformOrigin) :
        name(name),
        subGroupsNames(),
        transformOrigin(transformOrigin)
        { groupIndex ++; }
    
public:
    
    /**
     * Initialise un 'Group' avec un nom par d�faut,
     * et (0,0,0) un point d'ancrage
     */
    Group():
        name(getDefaultName()),
        subGroupsNames(),
        transformOrigin(Point(0,0,0))
        { groupIndex ++; }
    
    /**
     * Initialise une copie du groupe 'g'
     */
    Group(const Group& g):
        name(g.name),
        subGroupsNames(g.subGroupsNames),
        transformOrigin(g.transformOrigin)
        { groupIndex ++; }
    
    /**
     * Fonction � red�finir
     *
     * Retourne un clone de lui-m�me
     */
    virtual Group* clone() const = 0;
    
    /**
     * Retourne le nom (identifiant) du groupe
     */
    string getName() const { return this->name; }

    /**
     * D�finit le nom (identifiant) du groupe
     */
    void setName(const string name) { this->name = name; }
    
    /**
     * Retourne l'ensemble des noms des sous-groupes
     */
    StringSet getSubGroupsNames() const { return subGroupsNames; }
    
    /**
     * Retourne le point d'ancrage du groupe
     */
    Point getTransformOrigin() const { return transformOrigin; }
    
    /**
     * D�finit le point d'ancrage du groupe
     */
    void setTransformOrigin(Point p) { transformOrigin = p;}
    
    /**
     * Applique une transformation aux objets du groupe pr�sent dans
     * la sc�ne 'scene'
     *
     * La transformation � appliquer est l'interpolation lin�aire de la
     * transformation 'transform' (en fonction du pourcentage de compl�tion
     * 'fraction')
     */
    void apply(const Gscene &scene, double fraction, Transform transform);

};


#endif /* defined(__GRAY__Group__) */
