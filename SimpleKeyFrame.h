//
//  SimpleKeyFrame.h
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#ifndef __GRAY__SimpleKeyFrame__
#define __GRAY__SimpleKeyFrame__

#include <iostream>
#include <string>
#include "KeyFrame.h"
#include "ModelGroup.h"

#define max(a,b) \
({ __typeof__ (a) _a = (a); \
__typeof__ (b) _b = (b); \
_a > _b ? _a : _b; })

#define min(a,b) \
({ __typeof__ (a) _a = (a); \
__typeof__ (b) _b = (b); \
_a < _b ? _a : _b; })

/**
 * Repr�sente une KeyFrame (simple) :
 *
 * Elle d�finit l'indice de d�part, la dur�e et la fonction
 * de transition d'une animation.
 *
 * La fonction de transition est repr�sent�e
 * par une fonction d'easing ('EasingFunction'),
 * une transformation ('Transform'),
 * et un groupe sur lequel appliquer cette transformation ('Group')
 */
class SimpleKeyFrame : public KeyFrame {
protected:

	/**
	 * L'indice de la frame de d�part
	 */
    unsigned int start;

    /**
     * La dur�e de l'animation (en frame)
     */
    unsigned int duration;

    /**
     * Le nom du groupe � transformer
     */
    string groupName;

    /**
     * La matrice de transformation � appliquer
     * (lorsque l'animation est compl�te)
     */
    Transform transform;

    /**
     * La fonction d'acc�l�ration de l'animation
     */
    const EasingFunction& easing;
    
public:
    
    /**
     * Initialise une SimpleKeyFrame qui d�marre
     * � la frame 'start', pour une dur�e de 'duration' frame
     * qui appliquera au groupe 'group' une transformation
     * calcul�e � partir de l'interpolation de la
     * transformation 'transform' avec
     * la fonction d'acc�l�ration 'easing'
     */
    SimpleKeyFrame(unsigned int start,
                   unsigned int duration,
                   const Group& group,
                   Transform transform,
                   const EasingFunction& easing):
        start(start),
        duration(duration),
        groupName(group.getName()),
        transform(transform),
        easing(easing)
        {}
    

    /**
     * Initialise une copie de la SimpleKeyFrame 'k'
     */
    SimpleKeyFrame(const SimpleKeyFrame& k):
        start(k.start),
        duration(k.duration),
        groupName(k.groupName),
        transform(k.transform),
        easing(k.easing)
        {}
    
    /**
	 * voir KeyFrame.h
	 */
    KeyFrame* clone() { return new SimpleKeyFrame(*this); }
    
    /**
     * voir KeyFrame.h
     */
    unsigned int getStart() const { return start; }

    /**
	 * voir KeyFrame.h
	 */
    unsigned int getDuration() const { return duration; }

    /**
     * voir KeyFrame.h
     */
    string getGroupName() const { return groupName; }
    
    /**
     * voir KeyFrame.h
     */
    double getInterpolationFractionForFrame(int frame);
    
    /**
     * retourne la matrice de transformation finale
     */
    Transform getTransform() const { return transform; }
    
    /**
     * voir KeyFrame.h
     */
    void apply(const Gscene& scene, int frame);
};

#endif /* defined(__GRAY__SimpleKeyFrame__) */
