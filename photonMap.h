// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//
// Contributed by D. Bourguignon, B. Daene

#ifndef PHOTON_MAP_H_
#define PHOTON_MAP_H_

#include "ray.h"
#include "models.h"
#include "materials.h"
#include <vector>

#define DIM_X 0
#define DIM_Y 1
#define DIM_Z 2

class Gscene;

class Photon
{
    public :
    Photon ( const Point position, const Point direction, const Point normale, const Color color ) :
    pos ( position ), dir ( direction ), nor ( normale ), color ( color ) {};
    
    Point pos, dir, nor;
    Color color;
};

class KD_tree
{
    public :
    KD_tree() : balanced(false) {}
    
    void addPhoton ( Photon &photon )
    {
        balanced = false;
        photons.push_back ( photon );
    }
    void balance();
    std::vector<Photon> getKNearest ( Point &position, Point &normale, int K, float &maxDist2, float minCos = 0.99f );
    
    private :
    void balance ( std::vector<int> &tab, long node, long n, long r );
    
    struct KD_pair
    {
        public :
        KD_pair ( int index, float dist2 ) : index ( index ), dist2 ( dist2 ) {}
        int index;
        float dist2;
    };
    
    void getKNearest ( Point &position, Point &normale, int K, float minCos, float &maxDist2, long node, std::vector<KD_pair> &result );
    void addPhoton ( int photon, Point &position, Point &normale, int K, float minCos, float &maxDist2, std::vector<KD_pair> &result );
    
    std::vector<Photon> photons;
    
    class KD_node
    {
        public :
        int photon;
        char plane;
    };
    
    // Le fils gauche du noeud i est le noeud i*2+1, le droit est le noeud i*2+2.
    std::vector<KD_node> tree;
    bool balanced;
};

class PhotonMap
{
    public :
    PhotonMap ( float spotRadius, int nbPhotonsPerSpot, float typicalLightDist ) :
    nbRayPerLightTotal ( 0 ), spotRadius2 ( spotRadius*spotRadius ), nbPhotonsPerSpot ( nbPhotonsPerSpot ), typicalLightDist ( typicalLightDist ) {}
    unsigned long populate ( Gscene &scene, unsigned long nbRayPerLight );
    void shade ( Color & k, HitInfo & hitInfo );
    
    private :
    int addRay ( Ray &ray, Gscene &scene );
    
    unsigned long nbRayPerLightTotal;
    float spotRadius2;
    int nbPhotonsPerSpot;
    float typicalLightDist;
    float lightCoef; //Calculed when populated
    
    KD_tree map;
};






#endif /* PHOTON_MAP_H_ */

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
