//
//  TimeLine.h
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#ifndef __GRAY__TimeLine__
#define __GRAY__TimeLine__

#include <iostream>
#include "scene.h"
#include "KeyFrame.h"

typedef std::list<KeyFrame*> KeyFrameList;
typedef std::vector<KeyFrameList> FrameList;

/**
 * Cette classe sert � mod�liser une ligne du temps pour une sc�ne.
 *
 * Elle contient un vecteur 'frames' qui repr�sente chaque frame
 * (= image) de la sc�ne.
 *
 * Chaque 'frame' est en fait une liste de 'KeyFrame'
 *
 * Elle est responsable de fournir un objet 'scene' pour chaque
 * instant donn� dans la timeline.
 *
 */
class TimeLine {
private:

	/**
	 * Le nombre total de frame
	 */
    unsigned int framesCount;

    /**
     * La scene initiale (contenant les models,groupes,lampes, etc.)
     */
    Gscene& scene;

    /**
     * La liste de frame (liste de KeyFrame)
     */
    FrameList frames;
    
    /**
     * L'indice de la frame actuelle
     */
    unsigned int currentFrame;

    /**
     * La scene actuelle (ou null si pas encore g�n�r�e --> cache)
     */
    Gscene* currentScene;
    
public:
    
    /**
     * Initialise une 'TimeLine' de 'framesCount' frames
     * se basant sur la scene 'scene'
     */
    TimeLine(unsigned int framesCount, Gscene& scene) :
        framesCount(framesCount),
        scene(scene),
        frames(framesCount),
        currentFrame(0),
        currentScene(NULL)
    {}
    
    /**
     * Retourne l'indice de la frame actuelle
     */
    unsigned int getCurrentFrame() { return currentFrame; }

    /**
     * Retourne le nombre total de frame que la sc�ne poss�de
     */
    unsigned int getFramesCount() { return framesCount; }

    /**
     * Retourne la scene initiale
     */
    Gscene& getScene() { return scene; }

    /**
     * Retourne la scene (g�n�r�e) pour la frame actuelle
     */
    Gscene* getCurrentScene();
    
    /**
     * Ajoute une keyFrame � la TimeLine
     */
    void addKeyFrame(KeyFrame* keyFrame);
    
    /**
     * Retourne la scene (g�n�r�e) pour la frame 'frame'
     */
    Gscene* getSceneAtFrame(unsigned int frame);
    
    /**
     * Passe la timeline de la frame courante � la frame suivante
     *
     * retourne l'indice de la nouvelle frame courante
     * 			ou -1 en cas d'erreur
     */
    int goToNextFrame();

    /**
	 * Passe la timeline de la frame courante � la frame pr�c�dente
	 *
	 * retourne l'indice de la nouvelle frame courante
	 * 			ou -1 en cas d'erreur
	 */
    int goToPreviousFrame();

    /**
	 * Ajoute 'offset' � l'indicide de la sc�ne courante
	 *
	 * retourne l'indice de la nouvelle frame courante
	 * 			ou -1 en cas d'erreur
	 */
    int goToFrameOffset(int offset);

    /**
	 * D�place la timeline � l'indice 'frame'
	 *
	 * retourne l'indice de la nouvelle frame courante
	 * 			ou -1 en cas d'erreur
	 */
    int goToFrame(unsigned int frame);
};

#endif /* defined(__GRAY__TimeLine__) */
