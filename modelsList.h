// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef MODELSLIST_H_
#define MODELSLIST_H_

enum Model
{
  GSPHERE, GAMBIENT, GDISC, GMESHMODEL
};

#endif /* MODELSLIST_H_ */
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
