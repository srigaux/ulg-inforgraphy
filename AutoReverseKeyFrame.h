//
//  AutoReverseKeyFrame.h
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#ifndef __GRAY__AutoReverseKeyFrame__
#define __GRAY__AutoReverseKeyFrame__

#include <iostream>
#include "SimpleKeyFrame.h"

/**
 * voir SimpleKeyFrame.
 *
 * Applique une SimpleKeyFrame dans un sens
 * et g�n�re une deuxi�me SimpleKeyFrame qui est l'inverse
 * de la premi�re dans l'autre sens.
 */
class AutoReverseKeyFrame : public KeyFrame {

protected:

    /**
     * Pause avant d'entamer la reverseKeyFrame
     */
    unsigned int pause;
    
	/**
	 * SimpleKeyFrame donn�e
	 */
    SimpleKeyFrame normalKeyFrame;

    /**
     * SimpleKeyFrame inverse
     */
    SimpleKeyFrame reverseKeyFrame;
    
    /**
     * Retourne la simpleKeyFrame utilis�e � la frame
     * d'indice 'frame'
     */
    SimpleKeyFrame* getSimpleKeyFrame(unsigned int frame);
    
public:
    
    /**
	 * Initialise une SimpleKeyFrame qui d�marre
	 * � la frame 'start', pour une dur�e de 2 fois 'duration' frame
	 * qui appliquera au groupe 'group' une transformation
	 * calcul�e � partir de l'interpolation de la
	 * transformation 'transform' avec
	 * la fonction d'acc�l�ration 'easing'
	 * la fonction d'acc�l�ration inverse 'reverseEasing'
	 */
    AutoReverseKeyFrame(unsigned int start,
                        unsigned int duration,
                        const Group& group,
                        Transform transform,
                        const EasingFunction& easing,
                        const EasingFunction& reverseEasing):
        normalKeyFrame(start, duration, group, transform, easing),
        reverseKeyFrame(start + duration, duration, group, transform, reverseEasing)
        {}
    
    
    /**
	 * Initialise une SimpleKeyFrame qui d�marre
	 * � la frame 'start', pour une dur�e de 2 fois 'duration' + 'pause' frame
	 * qui appliquera au groupe 'group' une transformation
	 * calcul�e � partir de l'interpolation de la
	 * transformation 'transform' avec
	 * la fonction d'acc�l�ration 'easing'
	 */
    AutoReverseKeyFrame(unsigned int start,
                        unsigned int pause,
                        unsigned int duration,
                        const Group& group,
                        Transform transform,
                        const EasingFunction& easing):
    normalKeyFrame(start, duration, group, transform, easing),
    reverseKeyFrame(start + duration + pause, duration, group, transform, easing)
    {}
    
    
    /**
	 * Initialise une SimpleKeyFrame qui d�marre
	 * � la frame 'start', pour une dur�e de 2 fois 'duration' frame
	 * qui appliquera au groupe 'group' une transformation
	 * calcul�e � partir de l'interpolation de la
	 * transformation 'transform' avec
	 * la fonction d'acc�l�ration 'easing'
	 */
    AutoReverseKeyFrame(unsigned int start,
                        unsigned int duration,
                        const Group& group,
                        Transform transform,
                        const EasingFunction& easing):
        normalKeyFrame(start, duration, group, transform, easing),
        reverseKeyFrame(start + duration, duration, group, transform, easing)
        {}
    
    /**
     * Initialise une copie de l'AutoReverseKeyFrame 'k'
     */
    AutoReverseKeyFrame(const AutoReverseKeyFrame& k):
        normalKeyFrame(k.normalKeyFrame),
        reverseKeyFrame(k.reverseKeyFrame)
        {}
    
    /**
     * voir KeyFrame.h
     */
    KeyFrame* clone() { return new AutoReverseKeyFrame(*this); }
    
    /**
     * voir KeyFrame.h
     */
    unsigned int getStart() const {
        return normalKeyFrame.getStart();
    }
    
    /**
     * voir KeyFrame.h
     */
    unsigned int getDuration() const {
        return reverseKeyFrame.getEnd() - normalKeyFrame.getStart();
    }
    
    /**
     * voir KeyFrame.h
     */
    unsigned int getEnd() const {
        return reverseKeyFrame.getEnd();
    }
    
    /**
     * voir SimpleKeyFrame.h
     */
    string getGroup() const { return normalKeyFrame.getGroupName(); }
    
    /**
     * voir KeyFrame.h
     */
    double getInterpolationFractionForFrame(int frame);
    
    /**
     * voir SimpleKeyFrame.h
     */
    Transform getTransform() const { return normalKeyFrame.getTransform(); }
    
    /**
     * voir KeyFrame.h
     */
    void apply(const Gscene& scene, int frame);
    
};

#endif /* defined(__GRAY__AutoReverseKeyFrame__) */
