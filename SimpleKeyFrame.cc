//
//  SimpleKeyFrame.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#include "SimpleKeyFrame.h"
#include <assert.h>

double SimpleKeyFrame::getInterpolationFractionForFrame(int frame)
{
    if (frame <= (int)start)
        return 0.;

    if (frame >= (int)getEnd())
        return 1.;

    double fraction = max(0, (frame - (int)start) / (double)duration);

    fraction = easing.ease(frame, start, duration);
    return fraction;
}

void SimpleKeyFrame::apply(const Gscene &scene, int frame)
{
    double fraction = getInterpolationFractionForFrame(frame);

    Group * modelGroup = scene.getGroup(groupName);

    assert(modelGroup != NULL);
    
    modelGroup->apply(scene, fraction, transform);
}















