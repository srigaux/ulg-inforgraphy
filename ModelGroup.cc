//
//  ModelGroup.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#include "ModelGroup.h"


string ModelGroup::getDefaultName() const {
    return "Model" + Group::getDefaultName();
}

Group* ModelGroup::clone() const {
    return new ModelGroup(*this);
}

ModelGroup::ModelGroup(const Gmodel& model):
    Group(getDefaultName(), Point(0,0,0)),
    modelNames()
{
    addModel(model);
}

void ModelGroup::addModel(const Gmodel &model) {
    modelNames.insert(model.getName());
}

void ModelGroup::apply(const Gscene &scene, Transform t) {
    
    Point p = transformOrigin;
    Point r = t.apply(p);
    transformOrigin = r;
    
    for (StringSet::iterator it = modelNames.begin();
         it != modelNames.end(); it++)
        scene.getModel(*it)->transform(t);
    
    for (StringSet::iterator it = subGroupsNames.begin();
         it != subGroupsNames.end(); it++) {
        ModelGroup * group = (ModelGroup *)scene.getGroup(*it);
        group->apply(scene, t);
    }
}

