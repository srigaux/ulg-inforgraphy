// Gray - A simple ray tracing program
// Copyright (C) 2008-2014 Eric Bechet
//
// See the COPYING file for contributions and license information.
// Please report all bugs and problems to <bechet@cadxfem.org>.
//

#ifndef __GLFRAMEBUFFER_H_
#define __GLFRAMEBUFFER_H_

#include <ostream>
#include "framebuffer.h"

class MyGlWindow;

class gl_framebuffer : public basic_framebuffer
{
public:
  gl_framebuffer(size_t w_, size_t h_, TimeLine* tl) :
    basic_framebuffer(w_ ,h_ ,tl) {}
    
  virtual ~gl_framebuffer() {}
  void setogl ( MyGlWindow *w )
  {
    ogl=w;
  }
  virtual void get_coords ( float i,float j,Ray &r );
private:
  MyGlWindow *ogl;
};

#endif // __GLFRAMEBUFFER_H_
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
