//
//  AutoReverseKeyFrame.cpp
//  GRAY
//
//  Created by Sébastien Rigaux on 8/04/14.
//
//

#include "AutoReverseKeyFrame.h"

double AutoReverseKeyFrame::getInterpolationFractionForFrame(int frame) {
    
    return getSimpleKeyFrame(frame)->getInterpolationFractionForFrame(frame);
}

void AutoReverseKeyFrame::apply(const Gscene &scene, int frame) {
    
    SimpleKeyFrame * skf = getSimpleKeyFrame(frame);
    
    if (skf == &reverseKeyFrame)
        frame = skf->getEnd() - frame + skf->getStart();
        
    skf->apply(scene, frame);
}

SimpleKeyFrame* AutoReverseKeyFrame::getSimpleKeyFrame(unsigned int frame) {
    
    return (frame <= normalKeyFrame.getEnd())
                ? &normalKeyFrame
                : &reverseKeyFrame;
}