//
//  EasingFunction.h
//  GRAY
//
//  Created by Sébastien Rigaux on 4/04/14.
//
//

#ifndef __GRAY__EasingFunction__
#define __GRAY__EasingFunction__

#include <iostream>
#include <math.h>

typedef enum {
    EaseIn,
    EaseOut,
    EaseInOut,
} Ease;

#define EaseInvert(ease) (ease == EaseIn ? EaseOut : (ease == EaseOut ? EaseIn : EaseInOut))

/**
 * Repr�sente une fonction (math�matique) qui renvoie le pourcentage de compl�tude
 * de transformation en fonction du pourcentage d'avancement d'un animation
 *
 * http://easings.net/fr
 *
 */
class EasingFunction {
    
protected:
	/**
	 * Indique le easing de la fonction
	 */
    Ease easing;
    
    /**
     * Calcule le pourcentage pour un easeIn
     */
    virtual float easeIn(float fraction) const = 0;

    /**
     * Calcule le pourcentage pour un easeOut
     */
    virtual float easeOut(float fraction) const = 0;

    /**
     * Calcule le pourcentage pour un easeInOut
     */
    virtual float easeInOut(float fraction) const = 0;
    
public:
    
    /**
     * Initialise une EasingFunction en mode EaseInOut
     */
    EasingFunction() : easing(EaseInOut) {}

    /**
     * Initialise une EasingFunction en mode 'easing'
     */
    EasingFunction(Ease easing) : easing(easing) {}
    
    /**
     * Copy constructor
     */
    EasingFunction(const EasingFunction& easingFunction) : easing(easingFunction.easing) {}
    
    /**
     * Calcule le pourcentage de compl�tude de transformation
     * � effectuer pour une animation � la frame 'current'
     * d�marant � la frame start, et d'une dur�e de 'duration' frames.
     */
    float ease(float current,
               float start,
               float duration) const;
};

/**
 * Macro permettant de d�clarer les easingFunction de fa�on compacte
 */
#define EasingFunctionClassDefinition(name, ease) \
class name##EasingFunction : public EasingFunction { \
 \
float easeIn(float fraction) const; \
float easeOut(float fraction) const; \
float easeInOut(float fraction) const; \
 \
public: \
 \
name##EasingFunction() : EasingFunction(ease) {} \
name##EasingFunction(Ease ease) : EasingFunction(ease) {} \
 \
}

EasingFunctionClassDefinition(Linear, EaseIn);
EasingFunctionClassDefinition(Quadratic, EaseInOut);
EasingFunctionClassDefinition(Cubic, EaseInOut);
EasingFunctionClassDefinition(Quartic, EaseInOut);
EasingFunctionClassDefinition(Quintic, EaseInOut);
EasingFunctionClassDefinition(Sine, EaseInOut);
EasingFunctionClassDefinition(Circular, EaseInOut);
EasingFunctionClassDefinition(Exponential, EaseInOut);
EasingFunctionClassDefinition(Elastic, EaseOut);
EasingFunctionClassDefinition(Back, EaseOut);
EasingFunctionClassDefinition(Bounce, EaseOut);



#endif /* defined(__GRAY__EasingFunction__) */
